<?php

include("../../../init/gestion.php");

$sql = "SELECT DE_CODIGO CODIGO, DE_NOMBRE NOMBRE FROM DEPARTAMENTOS ORDER BY NOMBRE";

$return_arr = array();

$result = ibase_query($conexion, $sql);

while ($fila = ibase_fetch_row($result)) {
    $row_array['CODIGO'] = utf8_encode($fila[0]);
    $row_array['NOMBRE'] = utf8_encode($fila[0]) . " - " . utf8_encode($fila[1]);
    array_push($return_arr, $row_array);
}

// $array = array("result"=>$return_arr);

echo json_encode($return_arr);
// echo json_encode($array);


?>