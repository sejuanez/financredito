// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $('#fotos').on('hidden.bs.modal', function () {
        app.buscar()
    })


});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {
        ajax: false,

        idCuenta: "",

        cuenta: "",
        fechaIni: "",
        fechaFin: "",
        selectEstados: "todas",

        cuentaModal: "",
        titular: "",
        medidor: "",
        direccion: "",

        tablaDatos: [],

        dato: "",

        select1: [],
        selected: "",

        selectTP: [],
        selectedTP: "",

        selectEDP: [],
        selectedEDP: "",

        selectEDI: [],
        selectedEDI: "",

        selectUDP: [],
        selectedUDP: "",

        selectCDP: [],
        selectedCDP: "",

        selectSEP: [],
        selectedSEP: "",

        selectEDLA: [],
        selectedEDLA: "",

        selectUDM: [],
        selectedUDM: "",

        selectCM: [],
        selectedCM: "",

        selectCS: [],
        selectedCS: "",

        selectMDM: [],
        selectedMDM: "",

        selectCONT: [],
        selectedCONT: "",

        selectEFEC: [],
        selectedEFEC: "",

        selectEVEN: [],
        selectedEVEN: "",

        textModal: "",

        atendio: "",
        telefono: "",
        celular: "",
        correo: "",
        longitud: "",
        latitud: "",
        observacion: "",
        fechaEJ: "",
        horaEJ: "",

        respuestaTP: "",
        respuestaEDP: "",
        respuestaEDI: "",
        respuestaUDP: "",
        respuestaCDP: "",
        respuestaSEP: "",
        respuestaEDLA: "",
        respuestaUDM: "",
        respuestaCM: "",
        respuestaCS: "",
        respuestaMDM: "",
        respuestaCONT: "",
        respuestaEFECT: "",
        respuestaEVEN: "",

        rutaFotos: [],

        cuentaActual: [],

    },


    methods: {

        loadPreguntas: function () {

            let app = this;

            $.ajax({
                url: './request/getPreguntas.php',
                type: 'POST',
                data: {},

            }).done(function (response) {

                let datos = JSON.parse(response);

                app.selectTP = datos['TP'];
                app.selectEDP = datos['EDP'];
                app.selectEDI = datos['EDI'];
                app.selectUDP = datos['UDP'];
                app.selectCDP = datos['CDP'];
                app.selectSEP = datos['SEP'];
                app.selectEDLA = datos['EDLA'];
                app.selectUDM = datos['UDM'];
                app.selectCM = datos['CM'];
                app.selectCS = datos['CS'];
                app.selectMDM = datos['MDM'];
                app.selectCONT = datos['CONT'];
                app.selectEFEC = datos['EFEC'];
                app.selectEVEN = datos['EVEN'];


            }).fail(function (error) {

                console.log(error)

            });


        },

        loadRespuestas: function () {

            let app = this;

            $.ajax({
                url: './request/getRespuestas.php',
                type: 'POST',
                data: {
                    idCuenta: app.idCuenta
                },

            }).done(function (response) {

                let datos = JSON.parse(response);

                try {


                    for (let i = 0; i < app.selectTP.length; i++) {
                        if (app.selectTP[i].codigo === datos['TP'][0]['codigo']) {
                            app.selectedTP = app.selectTP[i];
                            break;
                        }
                    }

                    for (let i = 0; i < app.selectEDP.length; i++) {
                        if (app.selectEDP[i].codigo === datos['EDP'][0]['codigo']) {
                            app.selectedEDP = app.selectEDP[i];
                            break;
                        }
                    }

                    for (let i = 0; i < app.selectEDI.length; i++) {
                        if (app.selectEDI[i].codigo === datos['EDI'][0]['codigo']) {
                            app.selectedEDI = app.selectEDI[i];
                            break;
                        }
                    }

                    for (let i = 0; i < app.selectUDP.length; i++) {
                        if (app.selectUDP[i].codigo === datos['UDP'][0]['codigo']) {
                            app.selectedUDP = app.selectUDP[i];
                            break;
                        }
                    }

                    for (let i = 0; i < app.selectCDP.length; i++) {
                        if (app.selectCDP[i].codigo === datos['CDP'][0]['codigo']) {
                            app.selectedCDP = app.selectCDP[i];
                            break;
                        }
                    }

                    for (let i = 0; i < app.selectSEP.length; i++) {
                        if (app.selectSEP[i].codigo === datos['SEP'][0]['codigo']) {
                            app.selectedSEP = app.selectSEP[i];
                            break;
                        }
                    }

                    for (let i = 0; i < app.selectEDLA.length; i++) {
                        if (app.selectEDLA[i].codigo === datos['EDLA'][0]['codigo']) {
                            app.selectedEDLA = app.selectEDLA[i];
                            break;
                        }
                    }

                    for (let i = 0; i < app.selectUDM.length; i++) {
                        if (app.selectUDM[i].codigo === datos['UDM'][0]['codigo']) {
                            app.selectedUDM = app.selectUDM[i];
                            break;
                        }
                    }

                    for (let i = 0; i < app.selectCM.length; i++) {
                        if (app.selectCM[i].codigo === datos['CM'][0]['codigo']) {
                            app.selectedCM = app.selectCM[i];
                            break;
                        }
                    }

                    for (let i = 0; i < app.selectCS.length; i++) {
                        if (app.selectCS[i].codigo === datos['CS'][0]['codigo']) {
                            app.selectedCS = app.selectCS[i];
                            break;
                        }
                    }

                    for (let i = 0; i < app.selectMDM.length; i++) {
                        if (app.selectMDM[i].codigo === datos['MDM'][0]['codigo']) {
                            app.selectedMDM = app.selectMDM[i];
                            break;
                        }
                    }

                    for (let i = 0; i < app.selectCONT.length; i++) {
                        if (app.selectCONT[i].codigo === datos['CONT'][0]['codigo']) {
                            app.selectedCONT = app.selectCONT[i];
                            break;
                        }
                    }

                    for (let i = 0; i < app.selectEVEN.length; i++) {
                        if (app.selectEVEN[i].codigo === datos['EVEN'][0]['codigo']) {
                            app.selectedEVEN = app.selectEVEN[i];
                            break;
                        }
                    }

                    for (let i = 0; i < app.selectEFEC.length; i++) {
                        if (app.selectEFEC[i].codigo === datos['EFEC'][0]['codigo']) {
                            app.selectedEFEC = app.selectEFEC[i];
                            break;
                        }
                    }

                } catch (e) {

                }

            }).fail(function (error) {

                console.log(error)

            });

        },

        loadFotos: function (idCuenta) {

            let app = this;

            $.ajax({
                url: './request/getFotos.php',
                type: 'POST',
                data: {
                    idCuenta: idCuenta,
                },

            }).done(function (response) {
                console.log(response);

                app.rutaFotos = JSON.parse(response);

                //return response;
            }).fail(function (error) {

                console.log(error)

            });

        },

        cargar: function () {

            let app = this;

            $("#inputFileContainer").html("" +
                "<input type='file' id='archivo' name='archivo' class='' accept='.png'" +
                " autocomplete='nope'>");

            $("#archivo").trigger("click");

            $('#archivo')[0].files[0] = "";

            $("#archivo").change(function () {

                console.log('upload file')

                app.subeFoto();

            });

        },

        subeFoto: function () {


            var app = this;
            var formData = new FormData($('.formGuardar')[0]);

            formData.append('id', app.cuentaActual.id);
            formData.append('anio', app.cuentaActual.anio);
            formData.append('mes', app.cuentaActual.mes);
            formData.append('cuenta', app.cuentaActual.cuenta);

            /*
            for (var pair of formData.entries()) {
                console.log(pair[0] + ', ' + pair[1]);
            }
            */

            //hacemos la petición ajax
            $.ajax({
                url: './uploadFile.php',
                type: 'POST',
                // Form data
                //datos del formulario
                data: formData,

                //necesario para subir archivos via ajax
                cache: false,
                contentType: false,
                processData: false,
                //mientras enviamos el archivo
                beforeSend: function () {
                },
                success: function (data) {

                    console.log(data)

                    app.loadFotos(app.cuentaActual.id)

                },
                //si ha ocurrido un error
                error: function (FormData) {
                    ////console.log(data)
                }
            });

        },

        borrarFoto: function (foto) {

            let app = this;

            $.ajax({
                url: './request/deleteFoto.php',
                type: 'POST',
                data: {
                    id: foto.id,
                    acta: foto.acta,
                    num: foto.num,
                    ruta: foto.ruta,
                },

            }).done(function (response) {
                console.log(response);
                app.loadFotos(foto.id)

                //app.variable = JSON.parse(response)

                //return response;
            }).fail(function (error) {

                console.log(error)

            });


            console.log(foto)
        },

        verVisita: function (dato) {

            let app = this;

            app.resetCampos();

            app.textModal = "Legalizar Visita: " + dato.cuenta + " - " + dato.titular;
            app.idCuenta = dato.id;

            app.cuentaModal = dato.cuenta;
            app.titular = dato.titular;
            app.medidor = dato.medidor;
            app.direccion = dato.direccion;

            app.atendio = dato.persona_atendio;
            app.telefono = dato.telefono;
            app.celular = dato.celular;
            app.correo = dato.correo;
            app.longitud = dato.longitud;
            app.latitud = dato.latitud;
            app.observacion = dato.observacion;
            app.fechaEJ = dato.fechaE;
            app.horaEJ = dato.horaE;

            app.loadRespuestas();

            $('#addSalario').modal('show');
        },

        verFotos: function (dato) {

            let app = this;

            app.cuentaActual = dato;

            app.idCuenta = dato.id;

            console.log(app.idCuenta)

            app.cuentaModal = dato.cuenta;
            app.titular = dato.titular;
            app.medidor = dato.medidor;
            app.direccion = dato.direccion;

            app.loadFotos(dato.id);

            $('#fotos').modal('show');
        },

        resetModal: function () {

            let app = this;

            $('#addSalario').modal('hide');
            $('.formGuardar').removeClass('was-validated');


        },

        buscar: function (op) {

            let app = this;

            //app.fechaIni = "2019-02-16";
            //app.fechaFin = "2019-04-16";

            $.ajax({
                url: './request/getDatos.php',
                type: 'POST',
                data: {
                    cuenta: app.cuenta,
                    fechaIni: app.fechaIni,
                    fechaFin: app.fechaFin,
                    estado: app.selectEstados,
                },

            }).done(function (response) {

                try {
                    app.resetModal();
                    let datos = (response);

                    if (datos.length === 1) {

                        if (op != 1) {
                            app.verVisita(datos[0]);
                            app.tablaDatos = datos;
                        }

                    } else {
                        app.tablaDatos = datos;
                    }

                    $("#btnExportar a").attr("href",
                        "request/exportarDatos.php?cuenta=" + app.cuenta +
                        "&fechaIni=" + app.fechaIni +
                        "&fechaFin=" + app.fechaFin +
                        "&estado=" + app.selectEstados
                    );

                } catch (e) {

                    alertify.warning('Ingresa un numero de cuenta o un rango de fechas')

                }

            }).fail(function (error) {

                console.log(error)
                alertify.warning('Ingresa un numero de cuenta o un rango de fechas')

            });


        },

        actualizar: function () {

            //    6027421

            let app = this;

            console.log(app.horaEJ);

            if (
                app.selectedTP == "" ||
                app.selectedEDP == "" ||
                app.selectedEDI == "" ||
                app.selectedUDP == "" ||
                app.selectedCDP == "" ||
                app.selectedSEP == "" ||
                app.selectedEDLA == "" ||
                app.selectedUDM == "" ||
                app.selectedCM == "" ||
                app.selectedCS == "" ||
                app.selectedMDM == "" ||
                app.selectedEVEN == "" ||
                app.selectedEFEC == "" ||
                app.selectedCONT == "" ||
                app.atendio == "" ||
                app.telefono == "" ||
                app.celular == "" ||
                app.correo == "" ||
                app.longitud == "" ||
                app.latitud == "" ||
                app.observacion == "" ||
                app.fechaEJ == "" ||
                (app.horaEJ == "" || app.horaEJ == null)
            ) {
                $('.formGuardar').addClass('was-validated');
                alertify.warning('Selecciona e ingresa todos los campos')
            } else {

                var array = {
                    "TP": app.selectedTP,
                    "EDP": app.selectedEDP,
                    "EDI": app.selectedEDI,
                    "UDP": app.selectedUDP,
                    "CDP": app.selectedCDP,
                    "SEP": app.selectedSEP,
                    "EDLA": app.selectedEDLA,
                    "UDM": app.selectedUDM,
                    "CM": app.selectedCM,
                    "CS": app.selectedCS,
                    "MDM": app.selectedMDM,
                    "CONT": app.selectedCONT,
                    "EFEC": app.selectedEFEC,
                    "EVEN": app.selectedEVEN,
                };

                $.ajax({
                    url: './request/insertDatos.php',
                    type: 'POST',
                    data: {
                        idCuenta: app.idCuenta,
                        atendio: app.atendio,
                        telefono: app.telefono,
                        celular: app.celular,
                        correo: app.correo,
                        longitud: app.longitud,
                        latitud: app.latitud,
                        fechaEJ: app.fechaEJ,
                        horaEJ: app.horaEJ,
                        observacion: app.observacion,
                        respuestas: JSON.stringify(array),
                    },

                }).done(function (data) {

                    if (data == '1') {
                        alertify.success('Legalizado correctamente')
                        app.buscar(1)
                        app.resetModal();
                    }

                }).fail(function (data) {
                    console.log(data)
                });
            }

        },

        resetCampos: function () {

            let app = this;

            app.selectedTP = "";
            app.selectedEDP = "";
            app.selectedEDI = "";
            app.selectedUDP = "";
            app.selectedCDP = "";
            app.selectedSEP = "";
            app.selectedEDLA = "";
            app.selectedUDM = "";
            app.selectedCM = "";
            app.selectedCS = "";
            app.selectedMDM = "";

        },

        cancelar: function () {

            let app = this;

            app.cuenta = "";
            app.fechaIni = "";
            app.fechaFin = "";
            app.tablaDatos = "";

        }

    },


    watch: {},

    mounted() {

        this.loadPreguntas();

    },

});
