<?php
/**
 * Created by PhpStorm.
 * User: KIKE
 * Date: 06/01/2019
 * Time: 6.46
 */


session_start();

if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../index.php';
        </script>";
    exit();
}
header("Content-Type: application/json; charset=utf-8");
include("../../../init/gestion.php");

$cuenta = ($_POST['cuenta']);
$fechaIni = ($_POST['fechaIni']);
$fechaFin = ($_POST['fechaFin']);
$estado = ($_POST['estado']);

if ($cuenta != "" && $fechaIni != "" && $fechaFin != "") {

    $stmt = "Select * from CONSULTA_VISITAS('3' , '" . $cuenta . "', '" . $fechaIni . "', '" . $fechaFin . "')";

} else if ($cuenta != "" && ($fechaIni == "" && $fechaFin == "")) {

    $stmt = "Select * from CONSULTA_VISITAS('1' , '" . $cuenta . "', null, null)";

} else if ($cuenta == "" && ($fechaIni != "" && $fechaFin != "")) {

    if ($estado == 'todas') {
        $stmt = "Select * from CONSULTA_VISITAS('2' , null, '" . $fechaIni . "', '" . $fechaFin . "')";
    } else if ($estado == 'pendientes') {
        $stmt = "Select * from CONSULTA_VISITAS('5' , null, '" . $fechaIni . "', '" . $fechaFin . "')";
    } else if ($estado == 'ejecutadas') {
        $stmt = "Select * from CONSULTA_VISITAS('4' , null, '" . $fechaIni . "', '" . $fechaFin . "')";
    }


}

$query = ibase_prepare($stmt);
$result = ibase_execute($query);


$return_arr = array();


while ($fila = ibase_fetch_row($result)) {
    $row_array['id'] = ($fila[0]);
    $row_array['anio'] = ($fila[1]);
    $row_array['mes'] = ($fila[2]);
    $row_array['cuenta'] = ($fila[3]);
    $row_array['antiguedad'] = ($fila[4]);
    $row_array['deuda'] = ($fila[5]);
    $row_array['titular'] = utf8_encode($fila[6]);
    $row_array['direccion'] = utf8_encode($fila[7]);
    $row_array['ubicacion'] = utf8_encode($fila[8]);
    $row_array['estrato'] = ($fila[9]);
    $row_array['regional'] = utf8_encode($fila[10]);
    $row_array['medidor'] = ($fila[11]);
    $row_array['marca'] = ($fila[12]);
    $row_array['transformador'] = ($fila[13]);
    $row_array['ciclo'] = ($fila[14]);
    $row_array['barrio'] = utf8_encode($fila[15]);
    $row_array['tecnico'] = utf8_encode($fila[16]);
    $row_array['fechaL'] = ($fila[17]);
    $row_array['horaL'] = ($fila[18]);
    $row_array['observacion'] = utf8_encode($fila[19]);
    $row_array['fechaE'] = ($fila[20]);
    $row_array['horaE'] = ($fila[21]);
    $row_array['persona_atendio'] = utf8_encode($fila[22]);
    $row_array['telefono'] = ($fila[23]);
    $row_array['celular'] = ($fila[24]);
    $row_array['correo'] = utf8_encode($fila[25]);
    $row_array['longitud'] = ($fila[26]);
    $row_array['latitud'] = ($fila[27]);
    $row_array['tieneFotos'] = ($fila[28]);
    array_push($return_arr, $row_array);
}

// $array = array("result"=>$return_arr);

echo json_encode($return_arr);
// echo json_encode($array);



