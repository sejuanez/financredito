<?php

session_start();
if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}

ini_set('max_execution_time', 0);

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Pragma: no-cache");

header("Expires: 0");

echo "\xEF\xBB\xBF"; // UTF-8 BOM

include("../../../init/gestion.php");

$cuenta = ($_GET['cuenta']);
$fechaIni = ($_GET['fechaIni']);
$fechaFin = ($_GET['fechaFin']);
$estado = ($_GET['estado']);

$fi = date_format(date_create($fechaIni), 'd-m-Y');
$ff = date_format(date_create($fechaFin), 'd-m-Y');

$nombre = "";

if ($cuenta != "" && $fechaIni != "" && $fechaFin != "") {

    $nombre = "Visita(s) cuenta(" . $cuenta . ") desde " . $fi . " hasta " . $ff;

    $stmt = "Select * from CONSULTA_VISITAS('3' , '" . $cuenta . "', '" . $fechaIni . "', '" . $fechaFin . "')";

} else if ($cuenta != "" && ($fechaIni == "" && $fechaFin == "")) {

    $nombre = "Visita cuenta(" . $cuenta . ")";

    $stmt = "Select * from CONSULTA_VISITAS('1' , '" . $cuenta . "', null, null)";

} else if ($cuenta == "" && ($fechaIni != "" && $fechaFin != "")) {


    if ($estado == 'todas') {
        $stmt = "Select * from CONSULTA_VISITAS('2' , null, '" . $fechaIni . "', '" . $fechaFin . "')";
        $nombre = "Visitas desde " . $fi . " hasta " . $ff;
    } else if ($estado == 'pendientes') {
        $stmt = "Select * from CONSULTA_VISITAS('5' , null, '" . $fechaIni . "', '" . $fechaFin . "')";
        $nombre = "Visitas Pendientes desde " . $fi . " hasta " . $ff;
    } else if ($estado == 'ejecutadas') {
        $stmt = "Select * from CONSULTA_VISITAS('4' , null, '" . $fechaIni . "', '" . $fechaFin . "')";
        $nombre = "Visitas Ejecutadas desde " . $fi . " hasta " . $ff;
    }


}

$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$arrayDatos = array();

header("Content-Disposition: filename=" . $nombre . ".xls");

$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$return_arr = array();


$tabla = "<table>" .
    "<tr>" .
    "<th style='text-align: center; font-size: 1.1em' colspan='28'> $nombre </th>" .
    "</tr>" .
    "<tr class='cabecera'>" .
    "<th>ID</th>" .
    "<th>Año</th>" .
    "<th>Mes</th>" .
    "<th>Cuenta</th>" .
    "<th>Antiguedad</th>" .
    "<th>Valor Deuda</th>" .
    "<th>Titular</th>" .
    "<th>Direccion</th>" .
    "<th>Ubicacion</th>" .
    "<th>Estrato</th>" .
    "<th>Regional</th>" .
    "<th>Medidor</th>" .
    "<th>Marca</th>" .
    "<th>Transformador</th>" .
    "<th>Ciclo</th>" .
    "<th>Barrio</th>" .
    "<th>Tecnico</th>" .
    "<th>Fecha Legaliza</th>" .
    "<th>Hora Legaliza</th>" .
    "<th>Observacion</th>" .
    "<th>Fecha Ejecuta</th>" .
    "<th>Hora Ejecuta</th>" .
    "<th>Persona atendio</th>" .
    "<th>Telefono</th>" .
    "<th>Celular</th>" .
    "<th>Correo</th>" .
    "<th>Longitud</th>" .
    "<th>Latitud</th>" .
    "</tr>";


while ($fila = ibase_fetch_row($result)) {

    $tabla .= "<tr class='fila'>" .
        "<td>" . ($fila[0]) . "</td>" .
        "<td>" . ($fila[1]) . "</td>" .
        "<td>" . ($fila[2]) . "</td>" .
        "<td>" . ($fila[3]) . "</td>" .
        "<td>" . ($fila[4]) . "</td>" .
        "<td>" . intval($fila[5]) . "</td>" .
        "<td>" . ($fila[6]) . "</td>" .
        "<td>" . ($fila[7]) . "</td>" .
        "<td>" . ($fila[8]) . "</td>" .
        "<td>" . ($fila[9]) . "</td>" .
        "<td>" . ($fila[10]) . "</td>" .
        "<td>" . ($fila[11]) . "</td>" .
        "<td>" . ($fila[12]) . "</td>" .
        "<td>" . ($fila[13]) . "</td>" .
        "<td>" . ($fila[14]) . "</td>" .
        "<td>" . ($fila[15]) . "</td>" .
        "<td>" . ($fila[16]) . "</td>" .
        "<td>" . ($fila[17]) . "</td>" .
        "<td>" . ($fila[18]) . "</td>" .
        "<td>" . ($fila[19]) . "</td>" .
        "<td>" . ($fila[20]) . "</td>" .
        "<td>" . ($fila[21]) . "</td>" .
        "<td>" . ($fila[22]) . "</td>" .
        "<td>" . ($fila[23]) . "</td>" .
        "<td>" . ($fila[24]) . "</td>" .
        "<td>" . ($fila[25]) . "</td>" .
        "<td>" . ($fila[26]) . "</td>" .
        "<td>" . ($fila[27]) . "</td>" .
        "</tr>";

}

$tabla .= "</table>";

echo $tabla;

