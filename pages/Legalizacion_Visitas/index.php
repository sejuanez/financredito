<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/loading.css"/>
    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css"/>
    <style type="text/css">

        fieldset {
            min-width: 0;
            padding-left: 10px;
            padding-right: 10px;
            border: 1px solid #cccccc;
        }

        .iconVerde {
            color: #27b900;
        }


        .iconRojo {
            color: #e10000;
        }

        legend {
            font-size: .7em;
            font-weight: bold;
            width: inherit;
        }


        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera th {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }

        table tr.cabecera2 th {
            color: #000000;
            text-align: center;
            font-weight: normal;
            font-size: 1.1em;
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 50vh;
            overflow-y: scroll;
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            text-align: center;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: #444;
            line-height: 28px;
            text-align: center;
        }

    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Legaliza Visitas

            <span id="btnBuscar" v-show="true" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 80px;" @click="cancelar();">
					<i class="fa fa-undo" aria-hidden="true"></i> Limpiar
		    	</span>

            <span id="btnBuscar" v-show="true" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 80px;" @click="buscar();">
					<i class="fa fa-search" aria-hidden="true"></i> Buscar
		    	</span>

            <span v-show="tablaDatos.length != 0" id="btnExportar"
                  class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 80px;">
                <a style="text-decoration: none; color: #273b4f;">
                    <i aria-hidden="true" class="fa fa-download"></i> Exportar
                    </a>
            </span>

        </p>
    </header>

    <div v-if="ajax" class="loading">Loading&#8230;</div>

    <div class="container" style="padding-bottom: 5px; max-width: 95%;">
        <form enctype="multipart/form-data" class="formGuardar" v-show="false">
            <div class="col">
                <div class="form-group" id="inputFileContainer">
                </div>
            </div>
        </form>

    </div>

    <div class="container" style="margin-bottom: 8px">

        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-12 col-sm-4">
                <div class="form-label-group">
                    <input type="text" id="cuenta" class="form-control" placeholder="cuenta" required=""
                           autofocus=""
                           @keyup.enter="buscar()"
                           autocomplete="nope"
                           v-model="cuenta">
                    <label for="cuenta">Numero de cuenta</label>
                </div>
            </div>
        </div>

        <div class="row" style="margin-top: 7px">


            <div class="col"></div>


            <div class="col-12 col-sm-3">
                <fieldset>
                    <legend>&nbsp Fecha &nbsp</legend>
                    <div class="row">

                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <div class="input-group input-group-sm">
                                    <input type="date" id="fecha" class="form-control" required aria-label="fecha"
                                           v-model="fechaIni">
                                </div>
                            </div>
                        </div>

                    </div>
                    <br>

                </fieldset>

            </div>
            <div class="col-12 col-sm-3">
                <fieldset>
                    <legend>&nbsp Fecha Fin &nbsp</legend>
                    <div class="row">

                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <div class="input-group input-group-sm">
                                    <input type="date" id="fecha" class="form-control" required aria-label="fecha"
                                           v-model="fechaFin">
                                </div>
                            </div>
                        </div>

                    </div>
                    <br>

                </fieldset>

            </div>
            <div class="col-12 col-sm-3">
                <fieldset>
                    <legend>&nbsp Estados &nbsp</legend>
                    <div class="row">

                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <select v-model="selectEstados" id="selectTP">
                                    <option value="todas">Todas</option>
                                    <option value="pendientes">Pendientes</option>
                                    <option value="ejecutadas">Ejecutadas</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <br>

                </fieldset>

            </div>

            <div class="col"></div>

        </div>

    </div>

    <div class="container" style="max-width: 95%;">

        <div class="row">
            <div class="col-12 my-tbody">
                <table class="table table-sm table-hover" style="">
                    <thead class="fondoGris">

                    <tr class="cabecera">
                        <th class="text-center" width="100">Cuenta</th>
                        <th class="text-center" width="70">Año</th>
                        <th class="text-center" width="40">Mes</th>
                        <th class="text-center">Titular</th>
                        <th width="100" class="text-center">Regional</th>
                        <th width="100" class="text-center">Barrio</th>
                        <th width="80" class="text-center">Medidor</th>
                        <th class="text-center">Tecnico</th>
                        <th width="100" class="text-center">Fecha Ej</th>
                        <th width="100" class="text-center">+</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="item in tablaDatos">
                        <td width="100" v-text="item.cuenta"></td>
                        <td width="70" v-text="item.anio"></td>
                        <td width="40" v-text="item.mes"></td>
                        <td v-text="item.titular" style="word-wrap:break-word"></td>
                        <td width="100" v-text="item.regional"></td>
                        <td width="100" v-text="item.barrio" style="word-wrap:break-word"></td>
                        <td width="80" v-text="item.medidor"></td>
                        <td v-text="item.tecnico"></td>
                        <td width="100" v-text="item.fechaE"></td>
                        <td width="100" style="text-align: center;">
                            <i class="fa fa-pencil" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="verVisita(item)"></i>

                            <i :class="item.tieneFotos != null? 'fa fa-image iconVerde':'fa fa-image iconRojo' "
                               title=""
                               style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="verFotos(item)"></i>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>

    </div>


    <div id="addSalario" class="modal fade bd-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">

                    <div class="container">
                        <div class="row">
                            <div class="col-sm-3"><strong>Cuenta: </strong><label v-text="cuentaModal"></label></div>
                            <div class="col-sm-6"><strong>Titular: </strong><label v-text="titular"></label></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-3"><strong>Medidor: </strong><label v-text="medidor"></label></div>
                            <div class="col-sm-6"><strong>Direccion: </strong><label v-text="direccion"></label></div>
                        </div>
                    </div>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form enctype="multipart/form-data" class="formGuardar">

                    <div class="modal-body">

                        <form>

                            <div class="row">

                                <div class="col-4">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Tipo Predio </label>
                                        <select v-model="selectedTP" id="selectTP">
                                            <option value="">Selecciona...</option>
                                            <option v-for="item in selectTP" :value="item" :key="item.codigo">
                                                {{ item.descripcion }}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-4">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Estado Predio </label>
                                        <select v-model="selectedEDP" id="selectEDP">
                                            <option value="">Selecciona...</option>
                                            <option v-for="item in selectEDP" :value="item" :key="item.codigo">
                                                {{ item.descripcion }}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-4">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Estructura
                                            Inmueble </label>
                                        <select v-model="selectedEDI" id="selectEDI">
                                            <option value="">Selecciona...</option>
                                            <option v-for="item in selectEDI" :value="item" :key="item.codigo">
                                                {{ item.descripcion }}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-4">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Uso del Predio </label>
                                        <select v-model="selectedUDP" id="selectUDP">
                                            <option value="">Selecciona...</option>
                                            <option v-for="item in selectUDP" :value="item" :key="item.codigo">
                                                {{ item.descripcion }}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-4">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Condiciones del
                                            Predio </label>
                                        <select v-model="selectedCDP" id="selectCDP">
                                            <option value="">Selecciona...</option>
                                            <option v-for="item in selectCDP" :value="item" :key="item.codigo">
                                                {{ item.descripcion }}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-4">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Encontre
                                            Predio? </label>
                                        <select v-model="selectedSEP" id="selectSEP">
                                            <option value="">Selecciona...</option>
                                            <option v-for="item in selectSEP" :value="item" :key="item.codigo">
                                                {{ item.descripcion }}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-4">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Estado
                                            Acometida </label>
                                        <select v-model="selectedEDLA" id="selectEDLA">
                                            <option value="">Selecciona...</option>
                                            <option v-for="item in selectEDLA" :value="item" :key="item.codigo">
                                                {{ item.descripcion }}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-4">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Ubicacion
                                            Medidor </label>
                                        <select v-model="selectedUDM" id="selectUDM">
                                            <option value="">Selecciona...</option>
                                            <option v-for="item in selectUDM" :value="item" :key="item.codigo">
                                                {{ item.descripcion }}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-4">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Cantidad de
                                            Medidores </label>
                                        <select v-model="selectedCM" id="selectCM">
                                            <option value="">Selecciona...</option>
                                            <option v-for="item in selectCM" :value="item" :key="item.codigo">
                                                {{ item.descripcion }}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-4">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Contacto</label>
                                        <select v-model="selectedCONT" id="selectCONT">
                                            <option value="">Selecciona...</option>
                                            <option v-for="item in selectCONT" :value="item" :key="item.codigo">
                                                {{ item.descripcion }}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-4">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Efecto</label>
                                        <select v-model="selectedEFEC" id="selectEFEC">
                                            <option value="">Selecciona...</option>
                                            <option v-for="item in selectEFEC" :value="item" :key="item.codigo">
                                                {{ item.descripcion }}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-4">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Evento</label>
                                        <select v-model="selectedEVEN" id="selectEVEN">
                                            <option value="">Selecciona...</option>
                                            <option v-for="item in selectEVEN" :value="item" :key="item.codigo">
                                                {{ item.descripcion }}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-4">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Con Servicio? </label>
                                        <select v-model="selectedCS" id="selectCS">
                                            <option value="">Selecciona...</option>
                                            <option v-for="item in selectCS" :value="item" :key="item.codigo">
                                                {{ item.descripcion }}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-4">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Mas de un
                                            Medidor? </label>
                                        <select v-model="selectedMDM" id="selectMDM">
                                            <option value="">Selecciona...</option>
                                            <option v-for="item in selectMDM" :value="item" :key="item.codigo">
                                                {{ item.descripcion }}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-4">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Atendio </label>
                                        <input type="text" id="descripcion" name="descripcion_modal"
                                               autocomplete="nope"
                                               class="form-control" required aria-label="descripcion_modal"
                                               v-model="atendio">
                                    </div>
                                </div>


                            </div>

                            <div class="row">

                                <div class="col-2">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Telefono Fijo </label>
                                        <input type="text" id="descripcion" name="descripcion_modal"
                                               autocomplete="nope"
                                               class="form-control" required aria-label="descripcion_modal"
                                               v-model="telefono">
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Celular </label>
                                        <input type="text" id="descripcion" name="descripcion_modal"
                                               autocomplete="nope"
                                               class="form-control" required aria-label="descripcion_modal"
                                               v-model="celular">
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Correo
                                            Electronico </label>
                                        <input type="text" id="descripcion" name="descripcion_modal"
                                               autocomplete="nope"
                                               class="form-control" required aria-label="descripcion_modal"
                                               v-model="correo">
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Longitud </label>
                                        <input type="text" id="descripcion" name="descripcion_modal"
                                               class="form-control" required aria-label="descripcion_modal"
                                               v-model="longitud">
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Latitud </label>
                                        <input type="text" id="descripcion" name="descripcion_modal"
                                               autocomplete="nope"
                                               class="form-control" required aria-label="descripcion_modal"
                                               v-model="latitud">
                                    </div>
                                </div>


                            </div>


                            <div class="row">

                                <div class="col-3">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Fecha Ejecucion </label>
                                        <input type="date" id="descripcion" name="descripcion_modal"
                                               autocomplete="nope"
                                               class="form-control" required aria-label="descripcion_modal"
                                               v-model="fechaEJ">
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group input-group-sm">
                                        <label class="font-weight-bold" for="descripcion_modal">Hora Ejecucion</label>
                                        <input type="time" id="descripcion" name="descripcion_modal"
                                               autocomplete="nope"
                                               class="form-control" required aria-label="descripcion_modal"
                                               v-model="horaEJ">
                                    </div>
                                </div>


                                <div class="col-7">
                                    <div class="form-group">
                                        <label class="font-weight-bold"
                                               for="exampleFormControlTextarea3">Observaciones</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea3"
                                                  v-model="observacion"
                                                  rows="1">
                                        </textarea>
                                    </div>
                                </div>

                            </div>

                        </form>


                    </div>

                    <div class="modal-footer">
                        <button type="button" v-if="true" class="btn btn-secondary btn-sm" @click="resetModal()">
                            Cancelar
                        </button>

                        <button type="button" v-if="true" class="btn btn-primary btn-sm" @click="actualizar()">
                            Actualizar
                        </button>


                    </div>

                </form>

            </div>


        </div>
    </div>
    <div id="fotos" class="modal fade bd-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">

                    <div class="container">
                        <div class="row">
                            <div class="col-sm-3"><strong>Cuenta: </strong><label v-text="cuentaModal"></label></div>
                            <div class="col-sm-6"><strong>Titular: </strong><label v-text="titular"></label></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-3"><strong>Medidor: </strong><label v-text="medidor"></label></div>
                            <div class="col-sm-6"><strong>Direccion: </strong><label v-text="direccion"></label></div>
                        </div>
                    </div>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form enctype="multipart/form-data" class="formGuardar">

                    <div class="modal-body">

                        <form>

                            <div class="row">

                                <div class="col-sm-4" v-for="foto in rutaFotos" style="margin-bottom: 5px;">
                                    <div class="card">
                                        <a :href="foto.ruta" download>
                                            <img :src="foto.ruta"
                                                 class="card-img-top img-fluid"
                                                 alt="Responsive image">
                                        </a>
                                        <div class="card-body text-center">
                                            <a @click="borrarFoto(foto)" href="#" class="btn btn-danger btn-sm">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </form>


                    </div>

                    <div class="modal-footer">
                        <button type="button" v-if="true" class="btn btn-secondary btn-sm" data-dismiss="modal">
                            Cancelar
                        </button>

                        <button type="button" v-if="true" class="btn btn-primary btn-sm" @click="cargar()">
                            Cargar Evidencia
                        </button>

                    </div>

                </form>

            </div>


        </div>
    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
