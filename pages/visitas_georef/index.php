<?php
session_start();

if (!$_SESSION['user']/* && !$_SESSION['permiso']*/) {//si no se ha inciciado sesion y se esta accediendo directamente del navegador
    echo
    "<script>
            window.location.href='../inicio/index.php';
        </script>";
    exit();
}
?>

<!DOCTYPE html5>

<html>

<head>

    <meta charset="utf-8">

    <title>Ubicacion de clientes</title>


    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

    <link rel="stylesheet" type="text/css" href="css/estilo.css">

    <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)'/>

    <!--<link rel='stylesheet' href='https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
    <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css'/>


    <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

    <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="js/Chart.js"></script>

    <!--<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>-->
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7o0dvz8VLcNGtEld2hnV58UNugeFaIcs"></script>
    <script type="text/javascript" src="../../js/gmaps.js"></script>

    <script type="text/javascript">


        var hoy = new Date();
        var dias = ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"];
        var meses = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"];
        var dia = hoy.getDate();
        var mes = hoy.getMonth() + 1;
        var anio = hoy.getFullYear();

        var colors = [
            '#0008C5',
            '#FF6347',
            '#FFFF00',
            '#00034A',
            '#8A2BE2',
            '#6ce600',
            '#FF1493',
            '#FF8C00',
            '#9932CC',
            '#ADFF2F',
            '#FF0000',
            '#FFD700',
            '#008000',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#800080',
            '#00FF7F',
            '#FF6347',
            '#8A2BE2',
            '#FFFF00',
            '#9932CC',
            '#ADFF2F',
            '#FF8C00',
            '#FF1493',
            '#008000',
            '#FFD700',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#FF0000',
            '#800080',
            '#00FF7F',
            '#FF6347',
            '#FFFF00',
            '#8A2BE2',
            '#FF8C00',
            '#9932CC',
            '#FF1493',
            '#ADFF2F',
            '#008000',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#FFD700',
            '#0008C5',
            '#FF6347',
            '#FFFF00',
            '#00034A',
            '#8A2BE2',
            '#6ce600',
            '#FF1493',
            '#FF8C00',
            '#9932CC',
            '#ADFF2F',
            '#FF0000',
            '#FFD700',
            '#008000',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#800080',
            '#00FF7F',
            '#FF6347',
            '#8A2BE2',
            '#FFFF00',
            '#9932CC',
            '#ADFF2F',
            '#FF8C00',
            '#FF1493',
            '#008000',
            '#FFD700',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#FF0000',
            '#800080',
            '#00FF7F',
            '#FF6347',
            '#FFFF00',
            '#8A2BE2',
            '#FF8C00',
            '#9932CC',
            '#FF1493',
            '#ADFF2F',
            '#008000',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#FFD700',
            '#0008C5',
            '#FF6347',
            '#FFFF00',
            '#00034A',
            '#8A2BE2',
            '#6ce600',
            '#FF1493',
            '#FF8C00',
            '#9932CC',
            '#ADFF2F',
            '#FF0000',
            '#FFD700',
            '#008000',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#800080',
            '#00FF7F',
            '#FF6347',
            '#8A2BE2',
            '#FFFF00',
            '#9932CC',
            '#ADFF2F',
            '#FF8C00',
            '#FF1493',
            '#008000',
            '#FFD700',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#FF0000',
            '#800080',
            '#00FF7F',
            '#FF6347',
            '#FFFF00',
            '#8A2BE2',
            '#FF8C00',
            '#9932CC',
            '#FF1493',
            '#ADFF2F',
            '#008000',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#FFD700',
            '#FF0000'];


        var nodoActivo = null;
        var map;
        var limits;


        $(function () {


            function actualizar() {

                $('#tapMapa').trigger("click");


                let fechaIni = $('#fecha').datepicker('getDate').getFullYear() + "-" + ($('#fecha').datepicker('getDate').getMonth() + 1) + "-" + $('#fecha').datepicker('getDate').getDate();
                let fechaFin = $('#fechaFin').datepicker('getDate').getFullYear() + "-" + ($('#fechaFin').datepicker('getDate').getMonth() + 1) + "-" + $('#fechaFin').datepicker('getDate').getDate();
                let vendedor = $("#vendedores").val();
                //ajax getClientes
                $.ajax({

                    url: 'request/getDatos.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        fechaIni: fechaIni,
                        fechaFin: fechaFin,
                        vendedor: vendedor,
                    }

                }).done(function (repuesta) {

                    //(map.markers[index].infoWindow).open(map.map,map.markers[index]);
                    //nodoActivo=null;


                    map.removeMarkers();
                    if (repuesta.length > 0) {// si hay clientes para mostrar

                        let filas = "";
                        let arrayAux = [];
                        let arrayMap = [];
                        let arrayMapLength = 0;

                        console.log(repuesta)

                        for (var i = 0; i < repuesta.length; i++) {


                            filas += "<tr class='fila'>" +
                                "<td>" + repuesta[i].cuenta + "</td>" +
                                "<td>" + repuesta[i].anio + "</td>" +
                                "<td>" + repuesta[i].mes + "</td>" +
                                "<td>" + repuesta[i].titular + "</td>" +
                                "<td>" + repuesta[i].regional + "</td>" +
                                "<td>" + repuesta[i].barrrio + "</td>" +
                                "<td>" + repuesta[i].medidor + "</td>" +
                                "<td>" + repuesta[i].tecnico + "</td>" +
                                "<td>" + repuesta[i].fecha_ej + "</td>" +
                                "</tr>";


                            map.addMarker({
                                lat: repuesta[i].lat,
                                lng: repuesta[i].lon,
                                icon: 'img/si.png',
                                title: repuesta[i].raz,
                                infoWindow: {
                                    content: "" +
                                        "<h3 style='text-align: center' id='title-foto'> CUENTA: " + repuesta[i].cuenta + "</h3>" +
                                        "<hr>" +
                                        "<p id=''><strong> TITULAR: </strong>" + repuesta[i].titular + "</p>" +
                                        "<p id=''><strong> FECHA: </strong>" + repuesta[i].anio + " - " + repuesta[i].mes + "</p>" +
                                        "<p id=''><strong> REGIONAL: </strong>" + repuesta[i].regional + "</p>" +
                                        "<p id=''><strong> DIRECCION: </strong>" + repuesta[i].direccion + "</p>" +
                                        "<p id=''><strong> BARRIO: </strong>" + repuesta[i].barrrio + "</p>" +
                                        "<p id=''><strong> TECNICO: </strong>" + repuesta[i].tecnico + "</p>" +
                                        "<p id=''><strong> ATENDIO: </strong>" + repuesta[i].persona_atendio + "</p>" +
                                        "<hr>" +
                                        "<p id=''><strong> Tipo Predio: </strong>" + repuesta[i].TP + "</p>" +
                                        "<p id=''><strong> Estado Predio: </strong>" + repuesta[i].EDP + "</p>" +
                                        "<p id=''><strong> Estructura Inmueble: </strong>" + repuesta[i].EDI + "</p>" +
                                        "<p id=''><strong> Uso del Predio: </strong>" + repuesta[i].UDP + "</p>" +
                                        "<p id=''><strong> Condiciones del Predio: </strong>" + repuesta[i].CDP + "</p>" +
                                        "<p id=''><strong> Encontre Predio?: </strong>" + repuesta[i].SEP + "</p>" +
                                        "<p id=''><strong> Estado Acometida: </strong>" + repuesta[i].EDLA + "</p>" +
                                        "<p id=''><strong> Ubicacion Medidor: </strong>" + repuesta[i].UDM + "</p>" +
                                        "<p id=''><strong> Mas de un Medidor?: </strong>" + repuesta[i].MDM + "</p>" +
                                        "<p id=''><strong> Cantidad de Medidores: </strong>" + repuesta[i].CM + "</p>" +
                                        "<p id=''><strong> Con Servicio?: </strong>" + repuesta[i].CS + "</p>" +
                                        "<p id=''><strong> Contacto: </strong>" + repuesta[i].CONT + "</p>" +
                                        "<p id=''><strong> Efecto: </strong>" + repuesta[i].EFEC + "</p>" +
                                        "<p id=''><strong> Evento: </strong>" + repuesta[i].EVEN + "</p>"


                                }
                            });
                            limits.extend(new google.maps.LatLng(repuesta[i].lat, repuesta[i].lon));

                        }

                        map.fitBounds(limits);

                        $("#tabla-general tbody").html(filas);

                        $("#exportar a").attr("href", "request/exportarExcel.php?" +
                            "fechaIni=" + fechaIni +
                            "&fechaFin=" + fechaFin +
                            "&vendedor=" + vendedor);
                        $("#exportar").show();


                        $("#consultando").hide();
                        $("#consultar").show();
                    }//fin si hay tecnicos
                    else {
                        $("#tabla-general tbody").html("");
                        $("#exportar a").attr("href", "");
                        $("#exportar").hide();
                        $("#consultando").hide();
                        $("#consultar").show();
                        $("#alert").dialog("open");
                        $("#alert p").html("No hay nada para mostrar.");
                    }


                });//fin del ajax Clientes

            };


            function iniciarComponentes() {

                //ajax que carga la lista de vendedoress
                $.ajax({
                    url: 'request/getTecnicos.php',
                    type: 'POST',
                    dataType: 'json'
                }).done(function (repuesta) {
                    if (repuesta.length > 0) {
                        var options = "";
                        for (var i = 0; i < repuesta.length; i++) {
                            if (repuesta[i] != null)
                                options += "<option value='" + repuesta[i].codigo + "'>" + repuesta[i].nombre + "</option>";

                        }
                        $("#vendedores").html(options);
                        $('#vendedores').attr('disabled', false);
                    }
                });// fin del ajax vendedores


                $('#fecha').datepicker({
                    dateFormat: 'dd/mm/yy', //'yy/M/d',
                    minDate: '-30Y',
                    maxDate: '+30Y',
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1,
                    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
                        'Junio', 'Julio', 'Agosto', 'Septiembre',
                        'Octubre', 'Noviembre', 'Diciembre'
                    ],
                    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
                        'May', 'Jun', 'Jul', 'Ago',
                        'Sep', 'Oct', 'Nov', 'Dic'
                    ]

                });

                $('#fecha').datepicker("setDate", new Date());

                $('#fechaFin').datepicker({
                    dateFormat: 'dd/mm/yy', //'yy/M/d',
                    minDate: '-30Y',
                    maxDate: '+30Y',
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1,
                    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
                        'Junio', 'Julio', 'Agosto', 'Septiembre',
                        'Octubre', 'Noviembre', 'Diciembre'
                    ],
                    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
                        'May', 'Jun', 'Jul', 'Ago',
                        'Sep', 'Oct', 'Nov', 'Dic'
                    ]

                });

                $('#fechaFin').datepicker("setDate", new Date());

                $("#alert").dialog({
                    modal: true,
                    autoOpen: false,
                    resizable: false,
                    buttons: {
                        Ok: function () {
                            //$("#alert p").html("");
                            $(this).dialog("close");
                        }
                    }
                });

                $('#fecha').change(function () {
                    compararFechas();
                });

                $('#fechaFin').change(function () {
                    compararFechas();
                });


                function compararFechas() {


                    let fecha1 = new Date($('#fecha').datepicker('getDate').getFullYear(),
                        $('#fecha').datepicker('getDate').getMonth() + 1,
                        $('#fecha').datepicker('getDate').getDate());

                    let fecha2 = new Date($('#fechaFin').datepicker('getDate').getFullYear(),
                        $('#fechaFin').datepicker('getDate').getMonth() + 1,
                        $('#fechaFin').datepicker('getDate').getDate());

                    if (fecha1.getTime() > fecha2.getTime()) {
                        $("#alert").dialog("open");
                        $("#alert p").html("La fecha inicial no puede ser mayor que la fecha final");
                        $('#fechaFin').datepicker("setDate", new Date());
                        $('#fecha').datepicker("setDate", new Date());
                    }

                }

                $("#tabs").tabs({
                    show: {effect: "slide", duration: 200},
                    active: 1,
                    activate: function (event, ui) {
                        /*var ancho = +$("#div-grafico-tecnico").width();
                        var alto = +$("#div-grafico-tecnico").height();
                        $("#grafico-tecnico").highcharts().setSize(ancho,alto, false); */
                    }
                });

                google.maps.event.addDomListener(window, "resize", function () {

                    var center_lat = parseFloat(map.getCenter().lat()).toPrecision(4);
                    var center_lng = parseFloat(map.getCenter().lng()).toPrecision(5);
                    $("#container-map").css({width: '100%', height: "90%"});
                    $("#div-map").css({width: '100%', height: "90%"});
                    google.maps.event.trigger(map, "resize");
                    map.setCenter(center_lat, center_lng);

                });


                $('#vendedores').attr('disabled', true);

                $("#exportar").hide();
                $("#consultar").show();
                $("#consultando").hide();

                //ajax que carga la lista de vendedoress
                $.ajax({
                    url: 'request/getVendedor.php',
                    type: 'POST',
                    dataType: 'json'
                }).done(function (repuesta) {
                    if (repuesta.length > 0) {
                        var options = "";
                        for (var i = 0; i < repuesta.length; i++) {
                            if (repuesta[i] != null)
                                options += "<option value='" + repuesta[i].codigo + "'>" + repuesta[i].nombre + "</option>";

                        }
                        $("#vendedores").html(options);
                        $('#vendedores').attr('disabled', false);
                    }
                });// fin del ajax vendedores


                //ajax que carga la lista de vendedoress
                $.ajax({
                    url: 'request/getCategoria.php',
                    type: 'POST',
                    dataType: 'json'
                }).done(function (repuesta) {
                    if (repuesta.length > 0) {
                        var options = "";
                        for (var i = 0; i < repuesta.length; i++) {
                            if (repuesta[i] != null)
                                options += "<option value='" + repuesta[i].codigo + "'>" + repuesta[i].nombre + "</option>";

                        }
                        $("#categoria").html(options);
                        $('#categoria').attr('disabled', false);
                    }
                });// fin del ajax vendedores


                //listener del evento clic del boton consultar
                $('#consultar').click(function (e) {

                    e.preventDefault();
                    $(this).hide();
                    //$("#contenido").hide();
                    $("#consultando").show();

                    actualizar();
                });


            };//fin de iniciarComponentes()

            //===================================================================================

            function cargarMapa() {
                //GMaps.geolocate({
                //success: function(position){
                //milat=position.coords.latitude;
                //milon=position.coords.longitude;
                map = new GMaps({  // muestra mapa centrado en coords [lat, lng]
                    el: '#div-map',
                    lat: 8.749349, /*position.coords.latitude,*/
                    lng: -75.879568,/*position.coords.longitude,*/
                    zoom: 10
                });


                //$("#header").html("<div id='div-title'><h3>Ubicación de funcionarios</h3><h5>"+dias[hoy.getDay()]+", "+dia+" de "+meses[mes-1]+" de "+anio+"</h5></div><nav><ul id='menu'><li id='actualizar'><a href='#'><span class='ion-ios-loop-strong'></span><h6>Actualizar</h6></a></li><li id='reset'><a href='#' ><span class='ion-ios-reload'></span><h6>Reset</h6></a></li></ul></nav>");
                //$("#modal").show();
                //limits = new google.maps.LatLngBounds();
                limits = new google.maps.LatLngBounds();
                iniciarComponentes();
                /*},
                error: function(error){
                   alert('Fallo en la geolocalizacion:'+error.message);
                   return false;
                },
                not_supported:function(){
                  alert('Geolocalizacion no soportada');
                  return false;
                }
            });*/
            };

            //======================================================================================


            cargarMapa();

            //$("body").show();


        });//fin del onready
    </script>

</head>

<!--<body style="display:none">-->
<body>

<div id="alert" title="Mensaje">
    <p>No hay nada para mostrar.</p>
</div>

<header>
    <h3>Visitas</h3>
    <nav>
        <ul id="menu">

            <li id="exportar"><a href=""><span class="ion-ios-download-outline"></span><h6>exportar</h6></a></li>
            <li id="consultar"><a href=""><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
            <li id="consultando"><a href=""><span class="ion-load-d"></span><h6>consultando...</h6></a></li>

        </ul>
    </nav>
</header>


<div id='subheader'>
    <div id="div-form">
        <form id="form">

            <div>
                <label for="fecha">Fecha</label>
                <input type='text' id='fecha' readOnly/>
            </div>

            <div>
                <label for="fechaFin">Fecha Fin</label>
                <input type='text' id='fechaFin' readOnly/>
            </div>

            <div>
                <label>Visitador</label>
                <select id='vendedores'></select>
            </div>

            <!--
                        <div>
                            <label>Promotor</label>
                            <select id='vendedores'></select></div>

                        <div>
                            <label>Categoria</label>
                            <select id='categoria'></select>
                        </div>

                        <div>
                            <label>Manejante</label>
                            <select id="manejante">
                                <option value="">Todos...</option>
                                <option value="S">Si</option>
                                <option value="N">No</option>
                            </select>
                        </div>
            -->
        </form>
    </div>
</div>


<div id="contenido">
    <div id='tabs'>

        <ul>
            <li><a href="#tab1">1. <span class='titulo-tab'>Datos</span></a></li>
            <li><a id="tapMapa" href="#tab2">2. <span class='titulo-tab'>Mapa</span></a></li>
        </ul>


        <div id='tab1'>
            <table id='tabla-general'>
                <thead>
                <tr class="cabecera">
                    <td width="100" class="text-center">Cuenta</td>
                    <td width="70" class="text-center">Año</td>
                    <td width="40" class="text-center">Mes</td>
                    <td class="text-center">Titular</td>
                    <td width="100" class="text-center">Regional</td>
                    <td width="100" class="text-center">Barrio</td>
                    <td width="80" class="text-center">Medidor</td>
                    <td class="text-center">Tecnico</td>
                    <td width="100" class="text-center">Fecha Ej</td>
                </tr>
                </thead>
                <tbody></tbody>
            </table><!--End end tabla-->
        </div><!--End tab1-->

        <div id='tab2'>
            <div id="container-map">
                <div id="div-map"></div>
            </div>
        </div><!--End tab2-->


    </div><!--End tabs-->
</div><!--End contenido-->

</body>
</html>