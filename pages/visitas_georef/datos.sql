SET TERM ^ ;

create or alter procedure CONSULTA_VISITAS_GEO (
    EOPCION integer,
    EFI date,
    EFF date,
    ECOD_TEC varchar(30))
returns (
    ID integer,
    ANO integer,
    MES integer,
    CUENTA integer,
    ANTIGUEDAD integer,
    VALOR_DEUDA numeric(18,2),
    TITULAR varchar(100),
    DIRECCION varchar(100),
    UBICACION varchar(100),
    ESTRATO integer,
    REGIONAL varchar(100),
    MEDIDOR varchar(30),
    MARCA varchar(30),
    TRANSFORMADOR varchar(30),
    CICLO integer,
    BARRIO varchar(50),
    TE_NOMBRES varchar(40),
    FECHA_LEGALIZA date,
    HORA_LEGALIZA time,
    OBSERVACION varchar(200),
    FECHA_EJ date,
    HORA_EJ time,
    PERSONA_ATENDIO varchar(100),
    PERSONA_TELEFONO varchar(20),
    PERSONA_CELULAR varchar(20),
    PERSONA_CORREO varchar(100),
    LONGITUD numeric(18,10),
    LATITUD numeric(18,10),
    FV_ID integer,
    DESCRIPCION varchar(100),
    DESCRIPCION1 varchar(100),
    DESCRIPCION2 varchar(100),
    DESCRIPCION3 varchar(100),
    DESCRIPCION4 varchar(100),
    DESCRIPCION5 varchar(100),
    DESCRIPCION6 varchar(100),
    DESCRIPCION7 varchar(100),
    DESCRIPCION8 varchar(100),
    DESCRIPCION9 varchar(100),
    DESCRIPCION10 varchar(100),
    DESCRIPCION11 varchar(100),
    DESCRIPCION12 varchar(100),
    DESCRIPCION13 varchar(100))
as
BEGIN
    begin
    if(eopcion = 1) then
    
  FOR
    WITH RESPUESTAS AS (
        SELECT
                rgv.rgv_id,
                rgv.rgv_tabla,
                rgv.rgv_codigo,
                om.descripcion
        from resultado_gestion_visita rgv
        left join ot_multitabla om on om.tabla = rgv.rgv_tabla and om.codigo = rgv.rgv_codigo
        )

        select g.id,g.ano, g.mes, g.cuenta, g.antiguedad
                        , g.valor_deuda, g.titular, g.direccion, g.ubicacion, g.estrato, g.regional, g.medidor, g.marca, g.transformador, g.ciclo, g.barrio, t.te_nombres, g.fecha_legaliza, g.hora_legaliza,g.observacion, g.fecha_ej,g.hora_ej,
                        g.persona_atendio, g.persona_telefono, g.persona_celular, g.persona_correo, g.longitud, g.latitud, f.fv_id
                        , cpt.descripcion, edp.descripcion, edi.descripcion, udp.descripcion, cdp.descripcion , sep.descripcion
                        , edla.descripcion, udm.descripcion , CM.descripcion , CS.descripcion, MDM.descripcion , CONT.descripcion
                        , EFEC.descripcion, EVEN.descripcion
        from gestion_cobro_visitas g
        inner join tecnicos t on t.te_codigo = g.tecnico
        left join foto_visitas f on f.fv_id = g.id and f.fv_estado ='S'
        left join respuestas cpt on  cpt.rgv_tabla = 'TP' and cpt.rgv_id = g.id
        left join respuestas edp on  edp.rgv_tabla = 'EDP' and edp.rgv_id = g.id
        left join respuestas edi on  edi.rgv_tabla = 'EDI' and edi.rgv_id = g.id
        left join respuestas udp on  udp.rgv_tabla = 'UDP' and udp.rgv_id = g.id
        left join respuestas cdp on  cdp.rgv_tabla = 'CDP' and cdp.rgv_id = g.id
        left join respuestas sep on  sep.rgv_tabla = 'SEP' and sep.rgv_id = g.id
        left join respuestas edla on  edla.rgv_tabla = 'EDLA' and edla.rgv_id = g.id
        left join respuestas udm on  udm.rgv_tabla = 'UDM' and udm.rgv_id = g.id
        left join respuestas CM on  CM.rgv_tabla = 'CM' and CM.rgv_id = g.id
        left join respuestas CS on  CS.rgv_tabla = 'CS' and CS.rgv_id = g.id
        left join respuestas MDM on  MDM.rgv_tabla = 'MDM' and MDM.rgv_id = g.id
        left join respuestas CONT on  CONT.rgv_tabla = 'CONT' and CONT.rgv_id = g.id
        left join respuestas EFEC on  EFEC.rgv_tabla = 'EFEC' and EFEC.rgv_id = g.id
        left join respuestas EVEN on  EVEN.rgv_tabla = 'EVEN' and EVEN.rgv_id = g.id
        where g.cumplida ='S'
        and g.fecha_ej between :efi and :eff
    INTO :ID,
         :ANO,
         :MES,
         :CUENTA,
         :ANTIGUEDAD,
         :VALOR_DEUDA,
         :TITULAR,
         :DIRECCION,
         :UBICACION,
         :ESTRATO,
         :REGIONAL,
         :MEDIDOR,
         :MARCA,
         :TRANSFORMADOR,
         :CICLO,
         :BARRIO,
         :TE_NOMBRES,
         :FECHA_LEGALIZA,
         :HORA_LEGALIZA,
         :OBSERVACION,
         :FECHA_EJ,
         :HORA_EJ,
         :PERSONA_ATENDIO,
         :PERSONA_TELEFONO,
         :PERSONA_CELULAR,
         :PERSONA_CORREO,
         :LONGITUD,
         :LATITUD,
         :FV_ID,
         :DESCRIPCION,
         :DESCRIPCION1,
         :DESCRIPCION2,
         :DESCRIPCION3,
         :DESCRIPCION4,
         :DESCRIPCION5,
         :DESCRIPCION6,
         :DESCRIPCION7,
         :DESCRIPCION8,
         :DESCRIPCION9,
         :DESCRIPCION10,
         :DESCRIPCION11,
         :DESCRIPCION12,
         :DESCRIPCION13
  DO
  BEGIN
    SUSPEND;
  END
  end

  ------------------------------------------------------------------------------

      begin
    if(eopcion = 2) then
    
  FOR
    WITH RESPUESTAS AS (
        SELECT
                rgv.rgv_id,
                rgv.rgv_tabla,
                rgv.rgv_codigo,
                om.descripcion
        from resultado_gestion_visita rgv
        left join ot_multitabla om on om.tabla = rgv.rgv_tabla and om.codigo = rgv.rgv_codigo
        )

        select g.id,g.ano, g.mes, g.cuenta, g.antiguedad
                        , g.valor_deuda, g.titular, g.direccion, g.ubicacion, g.estrato, g.regional, g.medidor, g.marca, g.transformador, g.ciclo, g.barrio, t.te_nombres, g.fecha_legaliza, g.hora_legaliza,g.observacion, g.fecha_ej,g.hora_ej,
                        g.persona_atendio, g.persona_telefono, g.persona_celular, g.persona_correo, g.longitud, g.latitud, f.fv_id
                        , cpt.descripcion, edp.descripcion, edi.descripcion, udp.descripcion, cdp.descripcion , sep.descripcion
                        , edla.descripcion, udm.descripcion , CM.descripcion , CS.descripcion, MDM.descripcion , CONT.descripcion
                        , EFEC.descripcion, EVEN.descripcion
        from gestion_cobro_visitas g
        inner join tecnicos t on t.te_codigo = g.tecnico
        left join foto_visitas f on f.fv_id = g.id and f.fv_estado ='S'
        left join respuestas cpt on  cpt.rgv_tabla = 'TP' and cpt.rgv_id = g.id
        left join respuestas edp on  edp.rgv_tabla = 'EDP' and edp.rgv_id = g.id
        left join respuestas edi on  edi.rgv_tabla = 'EDI' and edi.rgv_id = g.id
        left join respuestas udp on  udp.rgv_tabla = 'UDP' and udp.rgv_id = g.id
        left join respuestas cdp on  cdp.rgv_tabla = 'CDP' and cdp.rgv_id = g.id
        left join respuestas sep on  sep.rgv_tabla = 'SEP' and sep.rgv_id = g.id
        left join respuestas edla on  edla.rgv_tabla = 'EDLA' and edla.rgv_id = g.id
        left join respuestas udm on  udm.rgv_tabla = 'UDM' and udm.rgv_id = g.id
        left join respuestas CM on  CM.rgv_tabla = 'CM' and CM.rgv_id = g.id
        left join respuestas CS on  CS.rgv_tabla = 'CS' and CS.rgv_id = g.id
        left join respuestas MDM on  MDM.rgv_tabla = 'MDM' and MDM.rgv_id = g.id
        left join respuestas CONT on  CONT.rgv_tabla = 'CONT' and CONT.rgv_id = g.id
        left join respuestas EFEC on  EFEC.rgv_tabla = 'EFEC' and EFEC.rgv_id = g.id
        left join respuestas EVEN on  EVEN.rgv_tabla = 'EVEN' and EVEN.rgv_id = g.id
        where g.cumplida ='S'
        and g.fecha_ej between :efi and :eff
        and g.tecnico = :ecod_tec
    INTO :ID,
         :ANO,
         :MES,
         :CUENTA,
         :ANTIGUEDAD,
         :VALOR_DEUDA,
         :TITULAR,
         :DIRECCION,
         :UBICACION,
         :ESTRATO,
         :REGIONAL,
         :MEDIDOR,
         :MARCA,
         :TRANSFORMADOR,
         :CICLO,
         :BARRIO,
         :TE_NOMBRES,
         :FECHA_LEGALIZA,
         :HORA_LEGALIZA,
         :OBSERVACION,
         :FECHA_EJ,
         :HORA_EJ,
         :PERSONA_ATENDIO,
         :PERSONA_TELEFONO,
         :PERSONA_CELULAR,
         :PERSONA_CORREO,
         :LONGITUD,
         :LATITUD,
         :FV_ID,
         :DESCRIPCION,
         :DESCRIPCION1,
         :DESCRIPCION2,
         :DESCRIPCION3,
         :DESCRIPCION4,
         :DESCRIPCION5,
         :DESCRIPCION6,
         :DESCRIPCION7,
         :DESCRIPCION8,
         :DESCRIPCION9,
         :DESCRIPCION10,
         :DESCRIPCION11,
         :DESCRIPCION12,
         :DESCRIPCION13
  DO
  BEGIN
    SUSPEND;
  END
  end

END^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT ON RESULTADO_GESTION_VISITA TO PROCEDURE CONSULTA_VISITAS_GEO;
GRANT SELECT ON OT_MULTITABLA TO PROCEDURE CONSULTA_VISITAS_GEO;
GRANT SELECT ON GESTION_COBRO_VISITAS TO PROCEDURE CONSULTA_VISITAS_GEO;
GRANT SELECT ON TECNICOS TO PROCEDURE CONSULTA_VISITAS_GEO;
GRANT SELECT ON FOTO_VISITAS TO PROCEDURE CONSULTA_VISITAS_GEO;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE CONSULTA_VISITAS_GEO TO SYSDBA;
