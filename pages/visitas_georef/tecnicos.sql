SET TERM ^ ;

create or alter procedure TECNICOS_BUSCA (
    EOPCION integer,
    ECOD varchar(20))
returns (
    TE_CODIGO type of RDB$309,
    TE_NOMBRES type of RDB$310,
    TE_DIRECCION type of RDB$311,
    TE_TELEFONO type of RDB$312,
    TE_CIUDAD type of RDB$313,
    TE_USU_SISTEMA type of RDB$314,
    TE_CUADRILLA type of RDB$1707,
    TE_TPO type of RDB$1434,
    TE_CEDULA type of RDB$2758,
    TE_ESTADO type of RDB$2759,
    TE_COD_SAC type of RDB$834)
as
begin
  begin
  if(eopcion = 1) then
  for select te_codigo,
             te_nombres,
             te_direccion,
             te_telefono,
             te_ciudad,
             te_usu_sistema,
             te_cuadrilla,
             te_tpo,
             te_cedula,
             te_estado,
             te_cod_sac
      from tecnicos
      into :te_codigo,
           :te_nombres,
           :te_direccion,
           :te_telefono,
           :te_ciudad,
           :te_usu_sistema,
           :te_cuadrilla,
           :te_tpo,
           :te_cedula,
           :te_estado,
           :te_cod_sac
  do
  begin
    suspend;
  end
  end
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT ON TECNICOS TO PROCEDURE TECNICOS_BUSCA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE TECNICOS_BUSCA TO SYSDBA;
