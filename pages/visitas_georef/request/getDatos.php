<?php
/**
 * Created by PhpStorm.
 * User: KIKE
 * Date: 06/01/2019
 * Time: 6.46
 */

ini_set('max_execution_time', 0); //300 seconds = 5 minutes
ini_set('memory_limit', '-1');

include("../../../init/gestion.php");
// include("gestion.php");


$fechaIni = ($_POST['fechaIni']);
$fechaFin = ($_POST['fechaFin']);
$vendedor = ($_POST['vendedor']);

if ($vendedor == "") {
    $stmt = "Select * from CONSULTA_VISITAS_GEO('1','" . $fechaIni . "', '" . $fechaFin . "', null)";
} else {
    $stmt = "Select * from CONSULTA_VISITAS_GEO('2', '" . $fechaIni . "', '" . $fechaFin . "', '" . $vendedor . "')";
}


$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$response = array();


while ($fila = ibase_fetch_row($result)) {
    $row_array['id'] = ($fila[0]);
    $row_array['anio'] = ($fila[1]);
    $row_array['mes'] = utf8_encode($fila[2]);
    $row_array['cuenta'] = utf8_encode($fila[3]);
    $row_array['antiguedad'] = utf8_encode($fila[4]);
    $row_array['vr_deuda'] = utf8_encode($fila[5]);
    $row_array['titular'] = (($fila[6]));
    $row_array['direccion'] = ($fila[7]);
    $row_array['ubicacion'] = ($fila[8]);
    $row_array['estrato'] = ($fila[9]);
    $row_array['regional'] = ($fila[10]);
    $row_array['medidor'] = ($fila[11]);
    $row_array['marca'] = ($fila[12]);
    $row_array['transformador'] = ($fila[13]);
    $row_array['ciclo'] = ($fila[14]);
    $row_array['barrrio'] = ($fila[15]);
    $row_array['tecnico'] = ($fila[16]);
    $row_array['fecha_legaliza'] = ($fila[17]);
    $row_array['hora_legaliza'] = ($fila[18]);
    $row_array['observacion'] = ($fila[19]);
    $row_array['fecha_ej'] = ($fila[20]);
    $row_array['hora_ej'] = ($fila[21]);
    $row_array['persona_atendio'] = ($fila[22]);
    $row_array['persona_telefono'] = ($fila[23]);
    $row_array['persona_celular'] = ($fila[24]);
    $row_array['persona_correo'] = ($fila[25]);
    $row_array['lon'] = utf8_encode($fila[26]);
    $row_array['lat'] = utf8_encode($fila[27]);
    $row_array['TP'] = utf8_encode($fila[29]);
    $row_array['EDP'] = utf8_encode($fila[30]);
    $row_array['EDI'] = utf8_encode($fila[31]);
    $row_array['UDP'] = utf8_encode($fila[32]);
    $row_array['CDP'] = utf8_encode($fila[33]);
    $row_array['SEP'] = utf8_encode($fila[34]);
    $row_array['EDLA'] = utf8_encode($fila[35]);
    $row_array['UDM'] = utf8_encode($fila[36]);
    $row_array['CM'] = utf8_encode($fila[37]);
    $row_array['CS'] = utf8_encode($fila[38]);
    $row_array['MDM'] = utf8_encode($fila[39]);
    $row_array['CONT'] = utf8_encode($fila[40]);
    $row_array['EFEC'] = utf8_encode($fila[41]);
    $row_array['EVEN'] = utf8_encode($fila[42]);
    array_push($response, $row_array);
}


echo json_encode($response);




