<?php
/*
session_start();
if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}
*/
ini_set('max_execution_time', 0);

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Pragma: no-cache");

header("Expires: 0");

echo "\xEF\xBB\xBF"; // UTF-8 BOM

include("../../../init/gestion.php");


$fechaIni = ($_GET['fechaIni']);
$fechaFin = ($_GET['fechaFin']);
$vendedor = ($_GET['vendedor']);


if ($vendedor == "") {
    $stmt = "Select * from CONSULTA_VISITAS_GEO('1','" . $fechaIni . "', '" . $fechaFin . "', null)";
} else {
    $stmt = "Select * from CONSULTA_VISITAS_GEO('2', '" . $fechaIni . "', '" . $fechaFin . "', '" . $vendedor . "')";
}

$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$arrayDatos = array();

header("Content-Disposition: filename= Reporte visitas desde " . $fechaIni . " Hasta " . $fechaFin . ".xls");

$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$return_arr = array();


$tabla = "<table>" .
    "<tr>" .
    "<th style='text-align: center' colspan='42'>Visitas desde $fechaIni Hasta $fechaFin </th>" .
    "</tr>" .
    "<tr class='cabecera'>" .
    "<th>ID</th>" .
    "<th>Año</th>" .
    "<th>Mes</th>" .
    "<th>Cuenta</th>" .
    "<th>Antiguedad</th>" .
    "<th>Valor Deuda</th>" .
    "<th>Titular</th>" .
    "<th>Direccion</th>" .
    "<th>Ubicacion</th>" .
    "<th>Estrato</th>" .
    "<th>Regional</th>" .
    "<th>Medidor</th>" .
    "<th>Marca</th>" .
    "<th>Transformador</th>" .
    "<th>Ciclo</th>" .
    "<th>Barrio</th>" .
    "<th>Tecnico</th>" .
    "<th>Fecha legalizacion</th>" .
    "<th>Hora Legalizacion</th>" .
    "<th>Observacion</th>" .
    "<th>Fecha Ejecucion</th>" .
    "<th>Hora Ejecucion</th>" .
    "<th>Persona Atendio</th>" .
    "<th>Persona Telefono</th>" .
    "<th>Persona Celular</th>" .
    "<th>Persona Correo</th>" .
    "<th>Longitud</th>" .
    "<th>Latitud</th>" .
    "<th>Tipo Predio</th>" .
    "<th>Estado Predio</th>" .
    "<th>Estructura Inmueble</th>" .
    "<th>Uso del Predio</th>" .
    "<th>Condiciones del predio</th>" .
    "<th>Encontre Predio?</th>" .
    "<th>Estado Acometida</th>" .
    "<th>Ubicacion Medidor</th>" .
    "<th>Mas de un medidor?</th>" .
    "<th>Cantidad de Medidores</th>" .
    "<th>Con servicio?</th>" .
    "<th>Contacto</th>" .
    "<th>Efecto</th>" .
    "<th>Evento</th>" .
    "</tr>";


while ($fila = ibase_fetch_row($result)) {


    $tabla .= "<tr class='fila'>" .
        "<td>" . utf8_encode($fila[0]) . "</td>" .
        "<td>" . utf8_encode($fila[1]) . "</td>" .
        "<td>" . utf8_encode($fila[2]) . "</td>" .
        "<td>" . utf8_encode($fila[3]) . "</td>" .
        "<td>" . utf8_encode($fila[4]) . "</td>" .
        "<td>" . utf8_encode($fila[5]) . "</td>" .
        "<td>" . utf8_encode($fila[6]) . "</td>" .
        "<td>" . utf8_encode($fila[7]) . "</td>" .
        "<td>" . utf8_encode($fila[8]) . "</td>" .
        "<td>" . utf8_encode($fila[9]) . "</td>" .
        "<td>" . utf8_encode($fila[10]) . "</td>" .
        "<td>" . utf8_encode($fila[11]) . "</td>" .
        "<td>" . utf8_encode($fila[12]) . "</td>" .
        "<td>" . utf8_encode($fila[13]) . "</td>" .
        "<td>" . utf8_encode($fila[14]) . "</td>" .
        "<td>" . utf8_encode($fila[15]) . "</td>" .
        "<td>" . utf8_encode($fila[16]) . "</td>" .
        "<td>" . utf8_encode($fila[17]) . "</td>" .
        "<td>" . utf8_encode($fila[18]) . "</td>" .
        "<td>" . utf8_encode($fila[19]) . "</td>" .
        "<td>" . utf8_encode($fila[20]) . "</td>" .
        "<td>" . utf8_encode($fila[21]) . "</td>" .
        "<td>" . utf8_encode($fila[22]) . "</td>" .
        "<td>" . utf8_encode($fila[23]) . "</td>" .
        "<td>" . utf8_encode($fila[24]) . "</td>" .
        "<td>" . utf8_encode($fila[25]) . "</td>" .
        "<td>" . utf8_encode($fila[26]) . "</td>" .
        "<td>" . utf8_encode($fila[27]) . "</td>" .
        "<td>" . utf8_encode($fila[29]) . "</td>" .
        "<td>" . utf8_encode($fila[30]) . "</td>" .
        "<td>" . utf8_encode($fila[31]) . "</td>" .
        "<td>" . utf8_encode($fila[32]) . "</td>" .
        "<td>" . utf8_encode($fila[33]) . "</td>" .
        "<td>" . utf8_encode($fila[34]) . "</td>" .
        "<td>" . utf8_encode($fila[35]) . "</td>" .
        "<td>" . utf8_encode($fila[36]) . "</td>" .
        "<td>" . utf8_encode($fila[39]) . "</td>" .
        "<td>" . utf8_encode($fila[37]) . "</td>" .
        "<td>" . utf8_encode($fila[38]) . "</td>" .
        "<td>" . utf8_encode($fila[40]) . "</td>" .
        "<td>" . utf8_encode($fila[41]) . "</td>" .
        "<td>" . utf8_encode($fila[42]) . "</td>" .
        "</tr>";


}

$tabla .= "</table>";

echo $tabla;

