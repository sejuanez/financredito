// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);
    });

    $(".selectMunicipio").select2().change(function (e) {
        var usuario = $(".selectMunicipio").val();
        app.onChangeMun(usuario);
    });

    $(".selectAño").select2().change(function (e) {
        var usuario = $(".selectAño").val();
        app.onChangeAño(usuario);
    });

    $(".selectMes").select2().change(function (e) {
        var usuario = $(".selectMes").val();
        app.onChangeMes(usuario);
    });

    $(".selectComercializador").select2().change(function (e) {
        var usuario = $(".selectComercializador").val();
        app.onChangeComercializador(usuario);
    });


});

// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {

        ajax: false,


        btn_cargando: false,
        btn_borrando: false,
        btn_cargar: false,
        btn_verificar: true,
        btn_verificando: false,
        btn_cancelar: false,
        infoArchivo: false,

        selectMunicipio: [],
        valorMun: "",

        selectAño: [],
        valorAño: "",

        selectMes: [],
        valorMes: "",

        selectComercializador: [],
        valorComercializador: "",

        tablaConsumos: [],
        arrayEexcel: [],
        camposTabla: [],
        arrayConsumos: [],
        arrayErrores: [],
        arrayTarifas: [],

        textErrores: "Errores",
        textConsumo: "Consumos",

        textVerificadosArchivo: "Verificados:",
        textErroresArchivo: "Errores:",
        textTotalArchivo: "Total:",


    },


    methods: {

        download: function () {


            var link = document.getElementById('download');
            link.click();
        },

        verificar: function (msg) {

            var app = this;
            //console.log(app.valorMun)
            //console.log(app.valorAño)
            //console.log(app.valorMes)
            //console.log(app.valorComercializador)


            if (
                app.valorMun == "" ||
                app.valorAño == "" ||
                app.valorMes == ""
            ) {

                alertify.error("Por favor selecciona todas las opciones")

            } else {

                app.btn_verificar = false;
                app.btn_verificando = true;


                app.disableSelect2('.selectMunicipio')
                app.disableSelect2('.selectAño')
                app.disableSelect2('.selectMes')
                app.disableSelect2('.selectComercializador')


                $.post('./request/verificaConsumo.php', {
                    mun: app.valorMun,
                    anio: app.valorAño,
                    mes: app.valorMes,
                    comer: app.valorComercializador,
                }, function (data) {

                    console.log(data)

                    var datos = jQuery.parseJSON(data);


                    if (datos['consumo'].length <= 0) {
                        alertify.success("No se ha importado un consumo para esta busqueda todavia")

                        app.btn_cargar = true;
                        app.loadTarifas();

                    } else {


                        if (msg != 1) {
                            alertify.warning("Existen registros de consumo para esta busqueda")
                        }

                        app.btn_borra = true;
                        app.tablaConsumos = datos['consumo'];
                        app.textConsumo = "Consumos: " + "(" + datos['cantidad'][0]['cantidad'] + ")";
                        app.infoArchivo = false;
                        app.btn_cargar = false;

                    }

                    app.btn_cancelar = true;
                    app.btn_verificar = false;
                    app.btn_cargando = false;
                    app.btn_verificando = false;

                });

            }


        },

        cargar: function () {

            $("#inputFileContainer").html("" +
                "<input type='file' id='archivo' name='archivo' class='' accept='.xlsx, .xls, .csv'" +
                "autocomplete='nope'>");

            $("#archivo").trigger("click");

            $('#archivo')[0].files[0] = "";

            $("#archivo").change(function () {

                console.log('upload file')

                app.subeExcel();

            });

        },

        leerExcel: function (excel) {

            var app = this;
            /* set up XMLHttpRequest */
            var url = excel;
            var oReq = new XMLHttpRequest();
            oReq.open("GET", url, true);
            oReq.responseType = "arraybuffer";

            oReq.onload = function (e) {
                var arraybuffer = oReq.response;

                /* convert data to binary string */
                var data = new Uint8Array(arraybuffer);
                var arr = new Array();
                for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
                var bstr = arr.join("");

                /* Call XLSX */
                var workbook = XLSX.read(bstr, {type: "binary"});

                /* DO SOMETHING WITH workbook HERE */
                var first_sheet_name = workbook.SheetNames[0];
                /* Get worksheet */
                var worksheet = workbook.Sheets[first_sheet_name];


                app.arrayEexcel = (XLSX.utils.sheet_to_json(worksheet, {header: 1}, {defval: ""}));
                app.enviarArray();

            };

            oReq.send();

        },

        subeExcel: function () {


            var app = this;
            var formData = new FormData($('.formGuardar')[0]);

            app.arrayErrores = [];
            app.arrayConsumos = [];
            app.arrayEexcel = [];

            //hacemos la petición ajax
            $.ajax({
                url: './uploadFile.php',
                type: 'POST',
                // Form data
                //datos del formulario
                data: formData,

                //necesario para subir archivos via ajax
                cache: false,
                contentType: false,
                processData: false,
                //mientras enviamos el archivo
                beforeSend: function () {
                },
                success: function (data) {

                    app.leerExcel("./temp/" + data)

                },
                //si ha ocurrido un error
                error: function (FormData) {
                    ////console.log(data)
                }
            });

        },

        contains: function (arr, key, val) {
            for (var i = 0; i < arr.length; i++) {
                if (arr[i][key] === val) return true;
            }
            return false;
        },

        enviarArray: function () {

            var app = this;

            $.ajax({
                url: './request/insertConsumo.php',
                type: 'POST',
                // Form data
                //datos del formulario
                data: {
                    array: app.arrayEexcel,
                },

            }).done(function (data) {

                app.ajax = false;

                let result = JSON.parse(data);
                app.tablaConsumos = result['totales'];
                app.arrayErrores = result['errores'];

                if (app.arrayErrores.length == 0) {
                    alertify.alert('Importado correctamente', 'Todos los registros fueron cargados')
                } else {
                    alertify.alert('Error', 'El archivo tiene errores o registros duplicados, revisa la pestaña Errores para mas información <br> <br> Ningun registro fue cargado')
                }

                app.arrayConsumos = [];
                app.limpiarTemp();


            }).fail(function (data) {

            });

        },

        limpiarTemp: function () {
            $.ajax({
                url: './deleteFile.php',
                type: 'POST',
                data: {},

            }).done(function (response) {

            }).fail(function (error) {

                console.log(error)

            });
        },


    },


    watch: {},

    mounted() {


    },

});
