<?php

define('MAX_SEGMENT_SIZE', 65535);
ini_set('max_execution_time', 0);
error_reporting(0);

session_start();

if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../index.php';
        </script>";
    exit();
}

include("../../../init/gestion.php");


$array = $_POST['array'];


$arrayTotales = array();
$arrayErrores = array();

$total = count($array) - 1;
$errores = 0;
$insertados = 0;

$trans = ibase_trans(IBASE_DEFAULT);
$flang = true;

for ($i = 1; $i < count($array); $i++) {


    $sql = "INSERT INTO gestion_cobro_visitas (
                                                     CUENTA,
                                                     ANTIGUEDAD,
                                                     VALOR_DEUDA,
                                                     TITULAR,
                                                     DIRECCION,
                                                     UBICACION,
                                                     ESTRATO,
                                                     REGIONAL,
                                                     MEDIDOR,
                                                     MARCA,
                                                     TRANSFORMADOR,
                                                     CICLO,
                                                     BARRIO,
                                                     USUARIO_CARGUE)
                                   values
                                    (
                                    '" . (($array[$i][0] == "") ? "" : $array[$i][0]) . "',
                                    '" . (($array[$i][1] == "") ? "" : $array[$i][1]) . "',
                                    '" . (($array[$i][2] == "") ? "" : $array[$i][2]) . "',
                                    '" . (($array[$i][3] == "") ? "" : utf8_decode($array[$i][3])) . "',
                                    '" . (($array[$i][4] == "") ? "" : utf8_decode($array[$i][4])) . "',
                                    '" . (($array[$i][5] == "") ? "" : utf8_decode($array[$i][5])) . "',
                                    '" . (($array[$i][6] == "") ? "" : utf8_decode($array[$i][6])) . "',
                                    '" . (($array[$i][7] == "") ? "" : $array[$i][7]) . "',
                                    '" . (($array[$i][8] == "") ? "" : utf8_decode($array[$i][8])) . "',
                                    '" . (($array[$i][9] == "") ? "" : $array[$i][9]) . "',
                                    '" . (($array[$i][10] == "") ? "" : $array[$i][10]) . "',
                                    '" . (($array[$i][11] == "") ? "" : $array[$i][11]) . "',
                                    '" . (($array[$i][12] == "") ? "" : utf8_decode($array[$i][12])) . "',
                                    '" . $_SESSION['user'] . "'
                                    )
                                   ";

    $result = ibase_query($trans, $sql);

    if ($result == 1) {
        $insertados++;
    } else {
        $flang = false;
        $errores++;
        $row_array['cuenta'] = $array[$i][0];
        $row_array['titular'] = $array[$i][3];
        $row_array['error'] = "" . str_replace('"', '', ibase_errmsg()) . "";
        array_push($arrayErrores, $row_array);

    }

}

$row_array2['total'] = $total;
$row_array2['errores'] = $errores;
$row_array2['insertados'] = $insertados;
array_push($arrayTotales, $row_array2);

$return = [
    "totales" => $arrayTotales,
    "errores" => $arrayErrores,
];

if (!$flang) {
    ibase_rollback($trans);
    echo json_encode($return);
} else {

    ibase_commit($trans);
    echo json_encode($return);

}






