<?php
    session_start();

    if (!$_SESSION['user']/* && !$_SESSION['permiso']*/) {//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
    }
?>

<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8">

    <title>Asignar órdenes</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

    <link rel="stylesheet" type="text/css" href="css/estilo.css">

    <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)'/>

    <link rel='stylesheet' href='css/tablet.css' type='text/css'
          media='only screen and (min-width:481px) and (max-width:831px)'/>

    <!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
    <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css'/>


    <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

    <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

    <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>


    <script type="text/javascript">

        function inputBusqueda() {
            var anio = document.getElementById("anio").value;
            var mes = document.getElementById("mes").value;
            if (anio.length == 4) {
                setMunicipio(anio, mes)
                $('#barrio').html('');
            } else {
                $('#municipio').html('')
                $('#barrio').html('')
                $("#municipio").attr("disabled", true);
                $("#barrio").attr("disabled", true);
            }
        }

        function setMunicipio(anio, mes) {

            console.log(anio, mes)

            $.ajax({
                url: 'request/getMunicipios.php',
                type: 'POST',
                dataType: 'json',
                data: {anio: anio, mes: mes}

            }).done(function (repuesta) {

                var tamaño = repuesta.length;

                console.log(tamaño)
                console.log(repuesta)

                if (tamaño > 0) {
                    var options = "";

                    for (var i = 0; i < tamaño; i++) {

                        if (tamaño >= 1) {
                            if (i == 0) {
                                options += "<option value='' selected disabled>" + 'Seleccionar...' + "</option>";
                                options += "<option value='" + repuesta[i].codigo + "'>" + repuesta[i].nombre + "</option>";
                            } else {
                                options += "<option value='" + repuesta[i].codigo + "'>" + repuesta[i].nombre + "</option>";
                            }
                        }
                    }

                    $("#municipio").html(options);
                    $("#municipio").attr("disabled", false);
                    //setCorregimiento(Dpto,$("#municipio").val());
                }

            });

        }

        function setBarrio(anio, mes, mun) {

            $.ajax({
                url: 'request/getBarrios.php',
                type: 'POST',
                dataType: 'json',
                data: {anio: anio, mes: mes, mun: mun}

            }).done(function (repuesta) {
                if (repuesta.length > 0) {
                    var options = "";

                    for (var i = 0; i < repuesta.length; i++) {
                        options += "<option value='" + repuesta[i].codigo + "'>" + repuesta[i].nombre + "</option>";
                    }

                    $("#barrio").html(options);
                    $("#barrio").attr("disabled", false);

                    //$('#consultar').show();
                }

            });


        };// fin de la funcion setBarrio

        function buscar() {

            var anio = $("#anio").val();
            var mes = $("#mes").val();
            var mun = $("#municipio").val();
            var barrio = $("#barrio").val();

            if (anio != "" && mes != "" && mun != null && barrio != null) {

                $("#consultar").hide();
                $("#asignar").hide();
                $("#consultando").show();

                //reseteamos el contenido de la tabla
                $("table tbody").html("");

                $("#div-tecnico").hide();
                $("#seccion-tabla").hide();
                $("#registros").val("0");
                $("#chequeados").val("0");


                console.log(barrio)

                $.ajax({
                    url: 'request/consultar.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        anio: anio,
                        mes: mes,
                        barrio: barrio
                    }

                }).done(function (repuesta) {


                    var filas = "";

                    if (repuesta.length > 0) {

                        for (var i = 0; i < repuesta.length; i++) {

                            filas +=
                                "<tr class='fila' name='" + repuesta[i].id + "'>" +
                                "<td><input type='checkbox' data-orden='" + repuesta[i].id + "'></td>" +
                                "<td>" + repuesta[i].id + "</td>" +
                                "<td>" + repuesta[i].codigo + "</td>" +
                                "<td>" + repuesta[i].nombre + "</td>" +
                                "<td>" + repuesta[i].direccion + "</td>" +
                                "<td>" + repuesta[i].estrato + "</td>" +
                                "<td>" + repuesta[i].medidor + "</td>" +
                                "<td>" + repuesta[i].municipio + "</td>" +
                                "<td>" + repuesta[i].barrio + "</td>" +
                                "</tr>";
                        }

                        $("table tbody").html(filas);


                        //listener del evento change para cada checkbox de la tabla

                        $("table input[type='checkbox']").on("change", function () {

                            //desmarcamos la casilla 'seleccionar todo'
                            $("#seleccionarTodo").prop('checked', false);

                            if ($(this).prop("checked")) {//si se marcó un acasilla de la tabla

                                //agregamos la clase 'marcado' a la casilla cliqueada
                                $(this).addClass('marcado');

                                //incrementamos en 1 la cantidad de casillas seleccionadas
                                $('#chequeados').val((+$('#chequeados').val()) + 1);
                            } else {
                                //removemos la clase 'marcado' a la casilla cliqueada
                                $(this).removeClass('marcado');

                                //decrementamos en 1 la cantidad de casillas seleccionadas
                                $('#chequeados').val((+$('#chequeados').val()) - 1);
                            }
                        });

                        $("#registros").val(repuesta.length);
                        $("#div-tecnico").show();
                        $("#seccion-tabla").show();
                        $("#asignar").show();


                    } else {

                        $("table tbody").html("<tr class='fila'><td colspan='12'>(vacío)</td></tr>");
                        $("#div-tecnico").hide();
                        $("#seccion-tabla").show();
                        $("#asignar").hide();


                    }
                    $("#consultando").hide();
                    $("#consultar").show();


                }).fail(function (e) {
                    console.log(e)
                });

            } else {

                $("#alert p").html("Debes seleccionar la fecha, municipio y barrio.");
                $("#alert").dialog("open");
            }


        }

        function loadFecha() {
            var fecha = new Date();
            var anio = fecha.getFullYear();
            var mes = fecha.getMonth() + 1;

            var app = this;

            $('#anio').val(anio);
            $('#mes').val(mes);
            $('#mes').trigger('change');
        }


        $(function () {

            $('#mes').on('change', function () {
                inputBusqueda();
                $('#municipio').html('')
                $('#barrio').html('')
                $("#municipio").attr("disabled", true);
                $("#barrio").attr("disabled", true);

                var anio = document.getElementById("anio").value;
                var mes = document.getElementById("mes").value;
            });
            $('#municipio').on('change', function () {
                var anio = document.getElementById("anio").value;
                var mes = document.getElementById("mes").value;
                var municipio = document.getElementById("municipio").value;

                setBarrio(anio, mes, municipio);
            });


            loadFecha();


            //$("#consultar").hide();

            $("#alert").dialog({
                modal: true,
                autoOpen: false,
                resizable: false,
                buttons: {
                    Ok: function () {
                        $("#alert p").html("");
                        $(this).dialog("close");
                    }
                }
            });


            $("#cargando").hide();

            $("#consultando").hide();
            $("#guardando").hide();

            $("#asignar").hide();
            $("#seccion-tabla").hide();
            $("#div-tecnico").hide();

            $('#departamento').attr('disabled', true);
            $('#municipio').attr('disabled', true);
            $('#corregimiento').attr('disabled', true);
            $('#barrio').attr('disabled', true);


            $("#txtFechaIni").datepicker({
                minDate: new Date(),
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy",
                dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
            });

            $("#txtFechaIni").datepicker("setDate", new Date());


            function setCorregimiento(Dpto, Mun) {
                $.ajax({
                    url: 'request/getCorregimientos.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {dpto: Dpto, mun: Mun}

                }).done(function (repuesta) {
                    if (repuesta.length > 0) {
                        var options = "";

                        for (var i = 0; i < repuesta.length; i++) {
                            options += "<option value='" + repuesta[i].codigo + "'>" + repuesta[i].nombre + "</option>";
                        }

                        $("#corregimiento").html(options);
                        $("#corregimiento").attr("disabled", false);
                        //setBarrio(Dpto, Mun, $("#corregimiento").val());
                    }

                });


            };// fin de la funcion setCorregimiento


            //listener del evento Change del checkbox seleccionarTodo

            $("#seleccionarTodo").on("change", function () {


                //capturamos los checkbox de la tabla detalles
                var allCheckbox = $("table input[type='checkbox']");


                if ($(this).prop("checked")) {// si se selecciono seleccionarTodo

                    //asignamos la cantidad de casillas marcadas
                    $('#chequeados').val((allCheckbox.length) / 2);

                    //chequeamos todos los checkbox de la tabla
                    for (var i in allCheckbox) {
                        allCheckbox[i].checked = true;

                    }

                    //agregamos la clase 'marcado' a todas las casillas de la tabla
                    $("table input[type='checkbox']").addClass('marcado');

                } else {// si se deselecciono seleccionarTodo

                    //reseteamos la cantidad de casillas marcadas
                    $('#chequeados').val(0);

                    //deschequeamos todos los checkbox de la tabla
                    for (var i in allCheckbox) {
                        allCheckbox[i].checked = false;
                    }

                    //removemos la clase 'marcado' a todas las casillas de la tabla
                    $("table input[type='checkbox']").removeClass('marcado');


                }//fin del else


            });//fin del listener change


            //listener del evento change para cada checkbox de la tabla

            $("table input[type='checkbox']").on("change", function () {

                //desmarcamos la casilla 'seleccionar todo'
                $("#seleccionarTodo").prop('checked', false);

                if ($(this).prop("checked")) {//si se marcó un acasilla de la tabla

                    //agregamos la clase 'marcado' a la casilla cliqueada
                    $(this).addClass('marcado');

                    //incrementamos en 1 la cantidad de casillas seleccionadas
                    $('#chequeados').val((+$('#chequeados').val()) + 1);
                } else {
                    //removemos la clase 'marcado' a la casilla cliqueada
                    $(this).removeClass('marcado');

                    //decrementamos en 1 la cantidad de casillas seleccionadas
                    $('#chequeados').val((+$('#chequeados').val()) - 1);
                }
            });


            //Listeners de el evento Change de las listas departamentos
            $("#departamento").on("change", function () {

                //$('#consultar').hide();

                $("#municipio").html("");
                $("#municipio").attr("disabled", true);

                $("#corregimiento").html("");
                $("#corregimiento").attr("disabled", true);

                $("#barrio").html("");
                $("#barrio").attr("disabled", true);

                setMunicipio($("#departamento").val());
            });
            //fin de Listeners de el evento Change de la lista departamentos

            //Listeners de el evento Change de las listas municipios
            $("#municipio").on("change", function () {

                //$('#consultar').hide();

                $("#corregimiento").html("");
                $("#corregimiento").attr("disabled", true);

                $("#barrio").html("");
                $("#barrio").attr("disabled", true);

                setCorregimiento($("#departamento").val(), $("#municipio").val());
            });
            //fin de Listeners de el evento Change de la lista municipio

            //Listeners de el evento Change de las listas corregimiento
            $("#corregimiento").on("change", function () {

                //$('#consultar').hide();

                $("#barrio").html("");
                $("#barrio").attr("disabled", true);

                setBarrio($("#departamento").val(), $("#municipio").val(), $("#corregimiento").val());
            });
            //fin de Listeners de el evento Change de la lista corregimiento


            //ajax que carga la lista de tecnicos
            $.ajax({
                url: 'request/getTecnicos.php',
                type: 'POST',
                dataType: 'json'

            }).done(function (repuesta) {

                if (repuesta.length > 0) {

                    var options = "";


                    for (var i = 0; i < repuesta.length; i++) {
                        options += "<option value='" + repuesta[i].codigo + "'>" + repuesta[i].nombre + "</option>";
                        //optionsMun+="<option value='"+repuesta[i].codigoMun+"'>"+repuesta[i].nombreMun+"</option>";
                    }

                    $("#tecnico").html(options);

                }


            });// fin del ajax departamentos


            //listener del evento clic del boton asignar

            $("#asignar").click(function (e) {

                e.preventDefault();

                if ((+$('#chequeados').val() > 0) && (document.getElementById('tecnico').selectedIndex > 0)) {

                    $("#cargando").show();

                    $("#consultar").hide();
                    $("#asignar").hide();
                    $("#guardando").show();


                    var marcados = $("table input[type='checkbox'].marcado");

                    var array_marcados = [];

                    for (i = 0; i < marcados.length; i++) {

                        array_marcados[i] = marcados[i].dataset['orden'];

                    }
                    //console.log(array_marcados);

                    var diaIni = +($('#txtFechaIni').datepicker('getDate').getDate());

                    if (diaIni < 10)
                        diaIni = "0" + diaIni;

                    var mesIni = +($('#txtFechaIni').datepicker('getDate').getMonth() + 1);
                    if (mesIni < 10)
                        mesIni = "0" + mesIni;

                    var fechaIni = $('#txtFechaIni').datepicker('getDate').getFullYear() + "-" + mesIni + "-" + diaIni;

                    var anio = $("#anio").val();
                    var mes = $("#mes").val();

                    $.post('./request/asignar.php', {
                        tecnico: $("#tecnico").val(),
                        ordenes: array_marcados,
                        fecha: fechaIni,
                        mes: mes,
                        anio: anio
                    }, function (repuesta) {

                        inputBusqueda();

                        console.log(repuesta)


                        for (i = 0; i < marcados.length; i++) {

                            $("input[data-orden='" + marcados[i].dataset['orden'] + "']").removeClass('marcado');
                            $("table tr[name='" + marcados[i].dataset['orden'] + "']").remove();
                        }

                        let resta = $("#registros").val() - marcados.length;

                        console.log(resta)

                        resta <= 0 ? $("#registros").val(0) : $("#registros").val(+$("#registros").val() - marcados.length);

                        let resta2 = $("#chequeados").val() - marcados.length;

                        resta <= 0 ? $("#chequeados").val(0) : $("#registros").val(+$("#chequeados").val() - marcados.length);

                        $("#seleccionarTodo").prop('checked', false);
                        document.getElementById('tecnico').selectedIndex = 0;


                        $("#guardando").hide();
                        $("#cargando").hide();
                        //alert("Ordenes asignadas correctamente");
                        $("#alert p").html("Ordenes asignadas correctamente.");
                        $("#alert").dialog("open");
                        $("#consultar").show();
                        $("#asignar").show();

                    });
                    /*
                    $.ajax({
                        url: 'request/asignar.php',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            tecnico: $("#tecnico").val(),
                            ordenes: array_marcados,
                            fecha: fechaIni,
                            mes: mes,
                            anio: anio
                        }
                    }).done(function (repuesta) {

                        console.log(repuesta)

                        for (i = 0; i < marcados.length; i++) {

                            $("input[data-orden='" + marcados[i].dataset['orden'] + "']").removeClass('marcado');
                            $("table tr[name='" + marcados[i].dataset['orden'] + "']").remove();
                        }
                        $("#registros").val(+$("#registros").val() - marcados.length);
                        $("#chequeados").val(+$("#chequeados").val() - marcados.length);
                        $("#seleccionarTodo").prop('checked', false);
                        document.getElementById('tecnico').selectedIndex = 0;


                        $("#guardando").hide();
                        $("#cargando").hide();
                        //alert("Ordenes asignadas correctamente");
                        $("#alert p").html("Ordenes asignadas correctamente.");
                        $("#alert").dialog("open");
                        $("#consultar").show();
                        $("#asignar").show();
                    });
                    */
                } else {
                    //alert("Debes escoger un tecnico y seleccionar las ordenes a asiganar");
                    $("#alert p").html("Debes escoger un tecnico y seleccionar las ordenes a asiganar.");
                    $("#alert").dialog("open");
                }

            });


            $("#guardando").click(function (e) {
                e.preventDefault();
            });

            $("body").show();

        });
    </script>
</head>
<body style="display:none">

<div id='cargando'>
    <progress></progress>
    <p>Espere...</p>
</div>

<div id="alert" title="Mensaje">
    <p>Ordenes asignadas correctamente</p>
</div>

<header>
    <h3>Asignar órdenes</h3>
    <nav>
        <ul id="menu">

            <li id="consultar" onclick="buscar()"><a><span class="ion-ios-search-strong"></span><h6>
                        consultar</h6></a></li>
            <li id="consultando"><a><span class="ion-load-d"></span><h6>consultando...</h6></a></li>
            <li id="asignar"><a><span class="ion-android-checkbox-outline"></span><h6>asignar</h6></a></li>
            <li id="guardando"><a><span class="ion-load-d"></span><h6>guardando...</h6></a></li>


        </ul>
    </nav>

</header>


<section
        style="padding-top: 44px; height: calc(100% - 80px); height: -webkit-calc(100% - 80px); height: -moz-calc(100% - 80px)">
    <div id='div-criterio'>
        <fieldset id='criterio'>
            <legend>Criterios de consulta</legend>


            <div>
                <div>
                    <label>Año</label>
                    <input type="text" id='anio' onkeyup="inputBusqueda()">
                </div>
            </div>

            <div>
                <div>
                    <label>Mes</label>
                    <select name="" id="mes">

                        <option value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>


                    </select>
                </div>
            </div>


            <div>
                <div>
                    <label>Regional</label>
                    <select id='municipio'>
                        <option value="x" disabled>Seleccione...</option>
                    </select>
                </div>
            </div>


            <div>
                <div>
                    <label>Barrio</label>
                    <select id='barrio'></select>
                </div>
            </div>

        </fieldset>
    </div>

    <!--<div id="div-tecnico">
        <div>
            <div>
                <label>Técnico</label>
                <select id='tecnico'></select>
            </div>
        </div>

    </div>-->


    <section id='seccion-tabla'>

        <div style="height: 100%;">
            <div id='header-tabla'>
                <div id='div-seleccionarTodo'><input type='checkbox' id='seleccionarTodo'><label>Sel. todo</label></div>
                <div id="div-tecnico">
                    <div>
                        <div>
                            <label>Técnico</label>
                            <select id='tecnico'></select>
                        </div>
                    </div>

                </div>
                <div id='div-fecha'><label>Ejecutar el</label><input type="text" id="txtFechaIni" readOnly></div>
                <div id='div-totales'><label>No. Chequeados</label><input type='text' id='chequeados' value='0'
                                                                          readOnly><label>No. Registros</label><input
                            type='text' id='registros' value='0' readOnly></div>
            </div>
            <div style="height: calc(100% - 24px); height: -webkit-calc(100% - 24px); height: -moz-calc(100% - 24px);">
                <table>
                    <thead>
                    <!--<tr class='cabecera'><td>Sel.</td><td>OT_Número</td><td>Cédula</td><td>Apellido1</td><td>Apellido2</td><td>Nombre1</td><td>Nombre2</td><td>Dpto</td><td>Municipio</td><td>Cgto</td></tr>-->
                    <tr class='cabecera'>
                        <td>Seleccionar</td>
                        <td>Id.</td>
                        <td>Cuenta</td>
                        <td>Titular</td>
                        <td>Direccion</td>
                        <td>Estrato</td>
                        <td>Medidor</td>
                        <td>Municipio</td>
                        <td>Barrio</td>
                    </tr>
                    </thead>

                    <tbody>
                    <tr class='fila'>
                        <td colspan='10'>(vacío)</td>
                    </tr>
                    </tbody>


                </table>
            </div>
        </div>

    </section>
</section>

<!--<footer><p><span class="ion-ios-information-outline"></span><em>Los campos del formulario marcados con * son obligatorios.</em></p></footer>-->

</body>
</html>
