// ----------------------------------------------
// On Load page
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });


    $(".selectBDUsuario").select2().change(function (e) {

        var usuario = $(this).val();
        var nombreUsuario = this.options[this.selectedIndex].text;


        app.onChangeTipoUsuario(usuario, nombreUsuario);

    });


    $("#foto").change(function () {
        readURL(this);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imgUsuario').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

});
// ----------------------------------------------
// ----------------------------------------------


let app = new Vue({
        el: '#app',
        data: {

            ajax: false,

            selectBDUsuarios: [],
            valorBDUsuario: "",

            idDepartamento: "",
            selectDepartamentos: [],

            idMunicipio: "",
            selectMunicipios: [],

            tablaDepartamentos: [],

            tablaPermisosTiene: [],
            tablaPermisosNoTiene: [],
            tablaTodos: [],


            usuario: "",
            nombreUsuario:
                "",

            password1:
                "",
            password2:
                "",


        },

        methods: {


            loadSelectUsuariosRegistrados: function () {
                var app = this;
                $.get('./request/getSelectUsuariosRegistrados.php', function (data) {

                    app.selectBDUsuarios = JSON.parse(data);

                });


            },


            onChangeTipoUsuario: function (usuario, nombreUsuario) {

                var app = this;

                console.log(usuario)
                console.log(nombreUsuario)


                app.usuario = usuario;
                app.nombreUsuario = usuario;


                app.tablaPermisosNoTiene = [];
                app.tablaPermisosTiene = [];


                if (nombreUsuario == "") {

                    $('#imgUsuario').attr('src', "./img/user.jpg");

                } else {

                    $('#imgUsuario').attr('src', "./request/getFoto.php?usuario=" + usuario);

                    // app.tablaDepartamentos = [];

                    $.get('./request/getClaveUsuario.php?usuario=' + usuario + '', function (data) {

                        console.log(data)

                        var datos = JSON.parse(data);

                        app.nombreUsuario = datos[0].NOMBRE;


                    });


                    $.get('./request/getPermisos.php?usuario=' + usuario, function (data) {

                        console.log(data)

                        var datos = JSON.parse(data);


                        app.tablaTodos = datos;


                        for (var i = 0; i < datos.length; i++) {

                            var usuario = datos[i].PERMISO;


                            if (usuario == "" || usuario == null) {

                                app.tablaPermisosNoTiene.push(datos[i]);

                            } else {

                                app.tablaPermisosTiene.push(datos[i]);

                            }

                        }


                        //app.tablaPermisos = JSON.parse(data);

                    });

                }

            }
            ,


            DarPermisos: function (index) {

                var app = this;
                var tablaNoTiene = app.tablaPermisosNoTiene;
                var tablaTiene = app.tablaPermisosTiene;

                var array = JSON.stringify(app.tablaPermisosNoTiene[index]);
                var formData = new FormData;
                formData.append('array', array);
                formData.append('usuario', app.usuario);


                //hacemos la petición ajax
                $.ajax({
                    url: './request/insertPermisos.php',
                    type: 'POST',
                    data: formData,
                    //necesario para subir archivos via ajax
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                    },
                    success: function (data) {

                        console.log((data));

                    },
                    //si ha ocurrido un error
                    error: function (FormData) {
                        console.log(data)
                    }
                });


                tablaTiene.push(tablaNoTiene[index]);
                tablaNoTiene.splice(index, 1);


            }
            ,

            QuitarPermisos: function (index) {

                var app = this;
                var tablaNoTiene = app.tablaPermisosNoTiene;
                var tablaTiene = app.tablaPermisosTiene;

                var array = JSON.stringify(app.tablaPermisosTiene[index]);
                var formData = new FormData;
                formData.append('array', array);
                formData.append('usuario', app.usuario);


                //hacemos la petición ajax
                $.ajax({
                    url: './request/eliminarPermisos.php',
                    type: 'POST',
                    data: formData,
                    //necesario para subir archivos via ajax
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                    },
                    success: function (data) {

                        console.log((data));

                    },
                    //si ha ocurrido un error
                    error: function (FormData) {
                        console.log(data)
                    }
                });

                tablaNoTiene.push(tablaTiene[index]);
                tablaTiene.splice(index, 1);


            }
            ,

            AgregarTodos: function () {

                var app = this;
                var tablaNoTiene = app.tablaPermisosNoTiene;
                var tablaTiene = app.tablaPermisosTiene;

                var array = JSON.stringify(app.tablaTodos);
                var formData = new FormData;
                formData.append('array', array);
                formData.append('usuario', app.usuario);


                //hacemos la petición ajax
                $.ajax({
                    url: './request/insertPermisos.php',
                    type: 'POST',
                    data: formData,
                    //necesario para subir archivos via ajax
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                    },
                    success: function (data) {

                        console.log((data));

                    },
                    //si ha ocurrido un error
                    error: function (FormData) {
                        console.log(formData)
                    }
                });

                app.tablaPermisosTiene = [];

                for (var i = 0; i < app.tablaTodos.length; i++) {
                    app.tablaPermisosTiene.push(app.tablaTodos[i]);
                }

                app.tablaPermisosNoTiene = [];


            },

            QuitarTodos: function () {

                var app = this;
                var tablaNoTiene = app.tablaPermisosNoTiene;
                var tablaTiene = app.tablaPermisosTiene;

                var array = JSON.stringify(app.tablaTodos);
                var formData = new FormData;
                formData.append('array', array);
                formData.append('usuario', app.usuario);


                //hacemos la petición ajax
                $.ajax({
                    url: './request/eliminarPermisos.php',
                    type: 'POST',
                    data: formData,
                    //necesario para subir archivos via ajax
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                    },
                    success: function (data) {

                        console.log((data));

                    },
                    //si ha ocurrido un error
                    error: function (FormData) {
                        console.log(formData)
                    }
                });


                app.tablaPermisosNoTiene = [];

                for (var i = 0; i < app.tablaTodos.length; i++) {
                    app.tablaPermisosNoTiene.push(app.tablaTodos[i]);
                }

                app.tablaPermisosTiene = [];

            },


        },

        watch:
            {}
        ,
        mounted() {

            this.loadSelectUsuariosRegistrados();

        }
        ,

    })
;
