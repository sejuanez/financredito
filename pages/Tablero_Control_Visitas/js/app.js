// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectMunicipio").select2().change(function (e) {
        var usuario = $(".selectMunicipio").val();
        app.onChangeMun(usuario);

    });

    $(".selectAño").select2().change(function (e) {
        var usuario = $(".selectAño").val();
        app.onChangeAño(usuario);

    });

    $(".selectMes").select2().change(function (e) {
        var usuario = $(".selectMes").val();
        app.onChangeMes(usuario);

    });

    $(".selectAñoFin").select2().change(function (e) {
        var usuario = $(".selectAñoFin").val();
        app.onChangeAñoFin(usuario);

    });

    $(".selectMesFin").select2().change(function (e) {
        var usuario = $(".selectMesFin").val();
        app.onChangeMesFin(usuario);

    });

    $(".selectComercializador").select2().change(function (e) {
        var usuario = $(".selectComercializador").val();
        app.onChangeComercializador(usuario);
        app.loadClientes();
    });

    $(".selectUsuario").select2().change(function (e) {
        var usuario = $(".selectUsuario").val();
        app.onChangeCliente(usuario);
    });


    $("#archivo").change(function () {

        app.subeExcel();

    });


});

// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {
        ajax: false,

        btn_cargando: false,
        btn_borrando: false,
        btn_cargar: false,
        btn_verificar: true,
        btn_verificando: false,
        btn_cancelar: false,

        fechaIni: "",
        fechaFin: "",

        tablaDetalle: [],
        graficoUno: [],
        graficoDos: [],
        graficoTres: [],
        graficoCuatro: [],

        labels: [],
        datasets: [],
        colors: [
            '#0008C5',
            '#FF6347',
            '#FFFF00',
            '#00034A',
            '#8A2BE2',
            '#6ce600',
            '#FF1493',
            '#FF8C00',
            '#9932CC',
            '#ADFF2F',
            '#FF0000',
            '#FFD700',
            '#008000',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#800080',
            '#00FF7F',
            '#FF6347',
            '#8A2BE2',
            '#FFFF00',
            '#9932CC',
            '#ADFF2F',
            '#FF8C00',
            '#FF1493',
            '#008000',
            '#FFD700',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#FF0000',
            '#800080',
            '#00FF7F',
            '#FF6347',
            '#FFFF00',
            '#8A2BE2',
            '#FF8C00',
            '#9932CC',
            '#FF1493',
            '#ADFF2F',
            '#008000',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#FFD700',
            '#0008C5',
            '#FF6347',
            '#FFFF00',
            '#00034A',
            '#8A2BE2',
            '#6ce600',
            '#FF1493',
            '#FF8C00',
            '#9932CC',
            '#ADFF2F',
            '#FF0000',
            '#FFD700',
            '#008000',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#800080',
            '#00FF7F',
            '#FF6347',
            '#8A2BE2',
            '#FFFF00',
            '#9932CC',
            '#ADFF2F',
            '#FF8C00',
            '#FF1493',
            '#008000',
            '#FFD700',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#FF0000',
            '#800080',
            '#00FF7F',
            '#FF6347',
            '#FFFF00',
            '#8A2BE2',
            '#FF8C00',
            '#9932CC',
            '#FF1493',
            '#ADFF2F',
            '#008000',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#FFD700',
            '#0008C5',
            '#FF6347',
            '#FFFF00',
            '#00034A',
            '#8A2BE2',
            '#6ce600',
            '#FF1493',
            '#FF8C00',
            '#9932CC',
            '#ADFF2F',
            '#FF0000',
            '#FFD700',
            '#008000',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#800080',
            '#00FF7F',
            '#FF6347',
            '#8A2BE2',
            '#FFFF00',
            '#9932CC',
            '#ADFF2F',
            '#FF8C00',
            '#FF1493',
            '#008000',
            '#FFD700',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#FF0000',
            '#800080',
            '#00FF7F',
            '#FF6347',
            '#FFFF00',
            '#8A2BE2',
            '#FF8C00',
            '#9932CC',
            '#FF1493',
            '#ADFF2F',
            '#008000',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#FFD700',
            '#FF0000'],

    },


    methods: {

        check: function (opcion) {
            var app = this;

            if (opcion === 1) {

                if (app.checkEsp) {
                    app.checkGen = true;
                    app.checkEsp = false;
                } else {
                    app.checkGen = false;
                }

            } else {

                if (app.checkGen) {
                    app.checkEsp = true;
                    app.checkGen = false;
                } else {
                    app.checkEsp = true;
                }


            }

        },

        onChangeMun: function (cod) {
            var app = this;
            app.valorMun = cod;
        },

        cancelar: function () {

            let app = this;

            app.btn_cargando = false;
            app.btn_borrando = false;
            app.btn_cargar = false;
            app.btn_verificar = true;
            app.btn_verificando = false;
            app.btn_cancelar = false;
            app.tablaDetalle = [];

            app.graficoUno = [];
            app.graficoDos = [];
            app.graficoTres = [];
            app.graficoCuatro = [];

            let ctx = document.getElementById('myChart1');
            let myChart = new Chart(ctx, {type: 'bar', data: null, options: null});
            myChart.destroy();

            let ctx2 = document.getElementById('myChart2');
            let myChart2 = new Chart(ctx2, {type: 'bar', data: null, options: null});
            myChart2.destroy();

            let ctx3 = document.getElementById('myChart3');
            let myChart3 = new Chart(ctx3, {type: 'bar', data: null, options: null});
            myChart3.destroy();

            let ctx4 = document.getElementById('myChart4');
            let myChart4 = new Chart(ctx4, {type: 'bar', data: null, options: null});
            myChart4.destroy();

        },

        verificar: function (msg) {

            var app = this;


            if (
                app.fechaIni === "" ||
                app.fechaFin === ""
            ) {

                alertify.error("Selecciona un rango de fechas")

            } else {

                app.btn_verificar = false;
                app.btn_verificando = true;


                $.post('./request/getDatos.php', {
                    fechaIni: app.fechaIni,
                    fechaFin: app.fechaFin,
                }, function (data) {

                    console.log(data);

                    try {

                        let datos = JSON.parse(data);

                        if (datos['datos'].length != 0) {

                            app.tablaDetalle = datos['datos'];
                            app.graficoUno = datos['grafico1'];
                            app.graficoDos = datos['grafico2'];
                            app.graficoTres = datos['grafico3'];
                            app.graficoCuatro = datos['grafico4'];
                            app.calcularGrafico();

                            $("#btnExportar a").attr("href",
                                "request/exportarDatos.php?fechaIni=" + app.fechaIni +
                                "&fechaFin=" + app.fechaFin
                            );

                        } else {
                            alertify.warning('No hay datos para esta busqueda')
                        }


                    } catch (e) {

                        console.log(e)

                    }


                    app.btn_cancelar = true;
                    app.btn_verificar = false;
                    app.btn_cargando = false;
                    app.btn_verificando = false;

                });


            }


        },

        calcularGrafico: function () {

            let app = this;

            let labels1 = [];
            let data1 = [];

            for (let i in app.graficoUno) {
                labels1.push(app.graficoUno[i].region);
                let cant = app.graficoUno[i].cant;
                data1.push(cant);
            }
            app.graficar('char1', 'myChart1', labels1, data1);


            let labels2 = [];
            let data2 = [];

            for (let i in app.graficoDos) {
                labels2.push(app.graficoDos[i].tecnico);
                let cant = app.graficoDos[i].cant;
                data2.push(cant);
            }
            app.graficar('char2', 'myChart2', labels2, data2);

            let labels3 = [];
            let data3 = [];

            for (let i in app.graficoTres) {
                labels3.push(app.graficoTres[i].fecha);
                let cant = app.graficoTres[i].cant;
                data3.push(cant);
            }
            app.graficar('char3', 'myChart3', labels3, data3);


            let labels4 = [];
            let data4 = [];

            for (let i in app.graficoCuatro) {
                labels4.push(app.graficoCuatro[i].antiguedad);
                let cant = app.graficoCuatro[i].cant;
                data4.push(cant);
            }
            app.graficar('char4', 'myChart4', labels4, data4);

        },

        graficar: function (div, char, labels, data) {

            var maxVal = parseInt((Math.max.apply(null, data)) * 1.25);

            while (true) {

                if (maxVal % 5 === 0) {
                    break;
                } else {
                    maxVal++;
                }
            }

            document.getElementById(div).innerHTML = "";
            document.getElementById(div).innerHTML = "<canvas style='margin-top: 35px;' id='" + char + "' ></canvas>";

            var ctx = document.getElementById(char);

            var myChart = new Chart(ctx, {
                showTooltips: false,
                type: 'bar',
                data: {
                    labels: labels,
                    datasets: [{
                        label: 'Cant',
                        data: data,
                        backgroundColor: app.colors,
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    aspectRatio: 1,
                    tooltips: {
                        enabled: true
                    },
                    legend: {
                        display: false
                    },
                    hover: {
                        animationDuration: 1
                    },
                    animation: {
                        duration: 1,
                        onComplete: function () {
                            var chartInstance = this.chart,
                                ctx = chartInstance.ctx;
                            ctx.textAlign = 'center';
                            ctx.fillStyle = "rgba(0, 0, 0, 1)";
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.controller.getDatasetMeta(i);
                                meta.data.forEach(function (bar, index) {
                                    var data = parseInt(dataset.data[index], 10);
                                    ctx.fillText(data, bar._model.x, bar._model.y - 5);

                                });
                            });
                        }
                    },


                    scales: {
                        yAxes: [{
                            ticks: {
                                min: 0,
                                max: parseInt((maxVal), 10)
                            },
                            scaleLabel: {
                                display: true,
                                labelString: "Porcentaje"
                            }
                        }],

                        xAxes: [{
                            gridLines: {
                                display: false
                            }
                        }]

                    }
                }
            });

        },

        loadFechas: function () {

            let app = this;

            var dt = new Date();
            var day = dt.getDate() < 10 ? '0' + dt.getDate() : dt.getDate();
            var month = (dt.getMonth() + 1) < 10 ? '0' + (dt.getMonth() + 1) : (dt.getMonth() + 1);
            var year = dt.getFullYear();
            app.fechaIni = year + '-' + month + '-' + day;
            app.fechaFin = year + '-' + month + '-' + day;


        },

    },


    watch: {},

    mounted() {

        this.loadFechas();

    },

});
