<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/loading.css"/>
    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">


    <style type="text/css">

        fieldset {
            min-width: 0;
            padding-left: 10px;
            padding-right: 10px;
            border: 1px solid #cccccc;
        }

        legend {
            font-size: .7em;
            font-weight: bold;
            width: inherit;
        }


        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera th {
            color: #fff;
            text-align: center;
            font-weight: normal;
            font-size: 1em;
        }

        table {
            font-size: 0.85em;
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 50vh;
            overflow-y: scroll;
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            text-align: center;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: #444;
            line-height: 28px;
            text-align: center;
        }

    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Tablero de control

            <span id="btnBuscar" v-show="btn_cancelar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;" @click="cancelar();">
					<i class="fa fa-user-times" aria-hidden="true"></i> Cancelar
		    	</span>

            <span id="btnBuscar" v-show="btn_verificar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;" @click="verificar();">
					<i class="fa fa-search" aria-hidden="true"></i> Consultar
		    	</span>

            <span id="btnBuscar" v-show="btn_verificando" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;">
					<i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Consultando
		    	</span>

            <span v-show="tablaDetalle.length != 0" id="btnExportar"
                  class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;">
                <a style="text-decoration: none; color: #273b4f;">
                    <i aria-hidden="true" class="fa fa-download"></i> Exportar
                    </a>
            </span>


        </p>
    </header>


    <div v-if="ajax" class="loading">Loading&#8230;</div>

    <div class="container">
        <div class="row" style="margin-top: 7px">


            <div class="col-sm-2"></div>

            <div class="col-12 col-sm-4">
                <fieldset>
                    <legend>&nbsp Fecha &nbsp</legend>
                    <div class="row">

                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <div class="input-group input-group-sm">
                                    <input type="date" id="fecha" class="form-control" required aria-label="fecha"
                                           v-model="fechaIni">
                                </div>
                            </div>
                        </div>

                    </div>
                    <br>

                </fieldset>

            </div>

            <div class="col-12 col-sm-4">
                <fieldset>
                    <legend>&nbsp Fecha Fin &nbsp</legend>
                    <div class="row">

                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <div class="input-group input-group-sm">
                                    <input type="date" id="fecha" class="form-control" required aria-label="fecha"
                                           v-model="fechaFin">
                                </div>
                            </div>
                        </div>

                    </div>
                    <br>

                </fieldset>

            </div>


        </div>
    </div>

    <div class="container" style="padding-top: 5px;padding-bottom: 5px; max-width: 95%;">

        <ul class="nav nav-tabs" id="myTab" role="tablist">

            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#datos" role="tab" aria-controls="home"
                   aria-selected="true">Datos</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#region" role="tab"
                   aria-controls="profile" aria-selected="false">Region</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#visitador" role="tab"
                   aria-controls="profile" aria-selected="false">Visitador</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#legaliza" role="tab"
                   aria-controls="profile" aria-selected="false">Fecha</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#antiguedad" role="tab"
                   aria-controls="profile" aria-selected="false">Antiguedad</a>
            </li>

        </ul>

        <div class="tab-content" id="myTabContent">

            <div class="tab-pane fade show active" id="datos" role="tabpanel" aria-labelledby="home-tab">
                <div class="container" style="max-width: 100%;">

                    <div class="container" style="max-width: 100%;margin-top: 8px;">

                        <div class="row">
                            <div class="col-12 my-tbody">
                                <table class="table table-sm table-hover" style="">
                                    <thead class="fondoGris">

                                    <tr class="cabecera">
                                        <th class="text-center" width="100">Cuenta</th>
                                        <th class="text-center">Titular</th>
                                        <th class="text-center">Direccion</th>
                                        <th width="100" class="text-center">Regional</th>
                                        <th width="100" class="text-center">Barrio</th>
                                        <th width="80" class="text-center">Medidor</th>
                                        <th class="text-center">Tecnico</th>
                                        <th width="100" class="text-center">Fecha Ej</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="item in tablaDetalle">
                                        <td width="100" v-text="item.cuenta"></td>
                                        <td v-text="item.titular" style="word-wrap:break-word"></td>
                                        <td v-text="item.direccion"></td>
                                        <td width="100" v-text="item.regional"></td>
                                        <td width="100" v-text="item.barrio"></td>
                                        <td width="80" v-text="item.medidor"></td>
                                        <td v-text="item.tecnico"></td>
                                        <td width="100" v-text="item.fecha"></td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>

                </div>


            </div>
            <div class="tab-pane fade" id="region" role="tabpanel" aria-labelledby="profile-tab">


                <div class="row">
                    <div id="char1" class="chart-container" style="position: relative; height:50vh; width:100vw">
                        <canvas style="margin-top: 35px;" id="myChart1"></canvas>
                    </div>
                </div>


            </div>
            <div class="tab-pane fade" id="visitador" role="tabpanel" aria-labelledby="profile-tab">


                <div class="row">
                    <div id="char2" class="chart-container" style="position: relative; height:50vh; width:100vw">
                        <canvas style="margin-top: 35px;" id="myChart2"></canvas>
                    </div>
                </div>


            </div>
            <div class="tab-pane fade" id="legaliza" role="tabpanel" aria-labelledby="profile-tab">


                <div class="row">
                    <div id="char3" class="chart-container" style="position: relative; height:50vh; width:100vw">
                        <canvas style="margin-top: 35px;" id="myChart3"></canvas>
                    </div>
                </div>


            </div>
            <div class="tab-pane fade" id="antiguedad" role="tabpanel" aria-labelledby="profile-tab">


                <div class="row">
                    <div id="char4" class="chart-container" style="position: relative; height:50vh; width:100vw">
                        <canvas style="margin-top: 35px;" id="myChart4"></canvas>
                    </div>
                </div>


            </div>

        </div>

    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/xlsx.full.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/Chart.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
