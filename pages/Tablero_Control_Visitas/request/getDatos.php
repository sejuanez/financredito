<?php
/**
 * Created by PhpStorm.
 * User: KIKE
 * Date: 06/01/2019
 * Time: 6.46
 */

ini_set('max_execution_time', 0); //300 seconds = 5 minutes
ini_set('memory_limit', '-1');

include("../../../init/gestion.php");
// include("gestion.php");


$fechaIni = ($_POST['fechaIni']);
$fechaFin = ($_POST['fechaFin']);

$stmt = "Select * from CONSULTA_VISITAS_TAB_CONTROL(
                                                1, 
                                                '" . $fechaIni . "', 
                                                '" . $fechaFin . "'
                                                )";

$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$arrayDatos = array();


while ($fila = ibase_fetch_row($result)) {
    $row_array['id'] = ($fila[0]);
    $row_array['cuenta'] = ($fila[1]);
    $row_array['titular'] = utf8_encode($fila[2]);
    $row_array['direccion'] = utf8_encode($fila[3]);
    $row_array['regional'] = utf8_encode($fila[4]);
    $row_array['barrio'] = ($fila[5]);
    $row_array['medidor'] = ($fila[6]);
    $row_array['tecnico'] = utf8_encode($fila[7]);
    $row_array['fecha'] = ($fila[8]);
    $row_array['hora'] = ($fila[9]);
    $row_array['observacion'] = utf8_encode($fila[10]);
    array_push($arrayDatos, $row_array);
}

//=============================================================================


$stmt2 = "Select * from CONSULTA_VISITAS_TAB_CONTROL(
                                                2, 
                                                '" . $fechaIni . "', 
                                                '" . $fechaFin . "'
                                                )";

$query2 = ibase_prepare($stmt2);
$result2 = ibase_execute($query2);

$graficoUno = array();


while ($fila = ibase_fetch_row($result2)) {
    $row_array['region'] = utf8_encode($fila[4]);
    $row_array['cant'] = ($fila[11]);
    array_push($graficoUno, $row_array);
}

//============================================================================

$stmt3 = "Select * from CONSULTA_VISITAS_TAB_CONTROL(
                                                3, 
                                                '" . $fechaIni . "', 
                                                '" . $fechaFin . "'
                                                )";

$query3 = ibase_prepare($stmt3);
$result3 = ibase_execute($query3);

$graficoDos = array();


while ($fila = ibase_fetch_row($result3)) {
    $row_array['tecnico'] = utf8_encode($fila[7]);
    $row_array['cant'] = ($fila[11]);
    array_push($graficoDos, $row_array);
}


//=============================================================================

$stmt4 = "Select * from CONSULTA_VISITAS_TAB_CONTROL(
                                                4, 
                                                '" . $fechaIni . "', 
                                                '" . $fechaFin . "'
                                                )";

$query4 = ibase_prepare($stmt4);
$result4 = ibase_execute($query4);

$graficoTres = array();


while ($fila = ibase_fetch_row($result4)) {
    $row_array['fecha'] = ($fila[8]);
    $row_array['cant'] = ($fila[11]);
    array_push($graficoTres, $row_array);
}

//=============================================================================

$stmt5 = "Select * from CONSULTA_VISITAS_TAB_CONTROL(
                                                5, 
                                                '" . $fechaIni . "', 
                                                '" . $fechaFin . "'
                                                )";

$query5 = ibase_prepare($stmt5);
$result5 = ibase_execute($query5);

$graficoCuatro = array();


while ($fila = ibase_fetch_row($result5)) {
    $row_array['antiguedad'] = "Antiguedad: " . ($fila[12]);
    $row_array['cant'] = ($fila[11]);
    array_push($graficoCuatro, $row_array);
}

// =============================================================================


$array = [
    "datos" => $arrayDatos,
    "grafico1" => $graficoUno,
    "grafico2" => $graficoDos,
    "grafico3" => $graficoTres,
    "grafico4" => $graficoCuatro,
];

echo json_encode($array);




