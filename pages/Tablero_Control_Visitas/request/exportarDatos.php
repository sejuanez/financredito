<?php
/*
session_start();
if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}
*/
ini_set('max_execution_time', 0);

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Pragma: no-cache");

header("Expires: 0");

echo "\xEF\xBB\xBF"; // UTF-8 BOM

include("../../../init/gestion.php");

$fechaIni = ($_GET['fechaIni']);
$fechaFin = ($_GET['fechaFin']);

$stmt = "Select * from CONSULTA_VISITAS_TAB_CONTROL(
                                                1, 
                                                '" . $fechaIni . "', 
                                                '" . $fechaFin . "'
                                                )";

$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$arrayDatos = array();

header("Content-Disposition: filename=Visitas desde " . $fechaIni . " Hasta " . $fechaFin . ".xls");

$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$return_arr = array();


$tabla = "<table>" .
    "<tr>" .
    "<th style='text-align: center' colspan='10'>Visitas Desde $fechaIni Hasta $fechaFin </th>" .
    "</tr>" .
    "<tr class='cabecera'>" .
    "<th>ID</th>" .
    "<th>Cuenta</th>" .
    "<th>Titular</th>" .
    "<th>Direccion</th>" .
    "<th>Regional</th>" .
    "<th>Barrio</th>" .
    "<th>Medidor</th>" .
    "<th>Tecnico</th>" .
    "<th>Fecha Ejecucion</th>" .
    "<th>Observaciones</th>" .
    "</tr>";


while ($fila = ibase_fetch_row($result)) {

    $tabla .= "<tr class='fila'>" .
        "<td>" . ($fila[0]) . "</td>" .
        "<td>" . ($fila[1]) . "</td>" .
        "<td>" . ($fila[2]) . "</td>" .
        "<td>" . ($fila[3]) . "</td>" .
        "<td>" . ($fila[4]) . "</td>" .
        "<td>" . ($fila[5]) . "</td>" .
        "<td>" . ($fila[6]) . "</td>" .
        "<td>" . ($fila[7]) . "</td>" .
        "<td>" . ($fila[8]) . "</td>" .
        "<td>" . ($fila[10]) . "</td>" .
        "</tr>";

}

$tabla .= "</table>";

echo $tabla;

