<?php
  session_start();

  //incluimos el archivo que contiene la configuracion de la conexion a la bd
  //include('../gestion.php');

  if(!$_SESSION['user']){//no se ha inciciado sesion
        
        //redireccionamos a la pagina principal
        echo
        "<script>
            window.location.href='../index.php';
        </script>";

        //salimos
        exit();
    }
    else //si se ha inciciado sesion
    {
    	//cerramos la sesion
    	$_SESSION['user']=null;
        $_SESSION['nombreUsuario']=null;

    	//redireccionamos a la pagina principal
    	echo
        "<script>
            window.location.href='../index.php';
        </script>";

        //salimos
        exit();
    }
 ?>